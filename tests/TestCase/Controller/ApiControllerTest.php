<?php
namespace App\Test\TestCase\Controller;

use Cake\TestSuite\IntegrationTestCase;

class ApiControllerTest extends IntegrationTestCase
{
    public $import = ['model' => 'Users'];
    public $fixtures = ['app.Users'];
    public $token = null;
    public $key = 'bd52a86f3305a0160fdfcc1ebf35ac32';

    /**
     * tests the login function of the API
     *
     * @return CakeResponse|null
     */
    public function testLogin()
    {
        // $this->loadFixtures('Users');
        $expected = json_decode('{
            "type": "fail",
            "code": 401,
            "message": "Login Failed",
            "data": null
        }', true);

        $key = $this->key;
        $this->configRequest([
            'headers' => [
                'Accept' => 'application/json',
                'X-Api-Key' => $key
            ]
        ]);
        $this->post('/api/login', [
            'email_address' => 'delgado.frenz@gmail.com',
            'password' => '123123'
        ]);

        $this->assertEquals(json_encode($expected), (string)$this->_response->getBody());

        return null;
    }

    /**
     * tests the home feed function of the API
     *
     * @return CakeResponse|null
     */
    public function testHome()
    {
        $key = $this->key;
        $token = $this->token;

        //no generated token
        $expected = json_decode('{
            "type": "fail",
            "code": 401,
            "message": "Unauthorize Access/Invalid Token",
            "data": null
        }', true);

        $this->configRequest([
            'headers' => [
                'Accept' => 'application/json',
                'X-Api-Key' => $key,
                'X-Api-Token' => $token
            ]
        ]);
        $result = $this->get('/api/home');

        $this->assertEquals(json_encode($expected), (string)$this->_response->getBody());

        return null;
    }

    /**
     * tests the logout function of the API
     *
     * @return CakeResponse|null
     */
    public function testLogout()
    {
        $key = $this->key;
        $token = $this->token;
        //no current user to be logged out
        $expected = json_decode('{
            "type": "fail",
            "code": 401,
            "message": "Unauthorize Access/Invalid Token",
            "data": null
        }', true);
        $this->configRequest([
            'headers' => [
                'Accept' => 'application/json',
                'X-Api-Key' => $key,
                'X-Api-Token' => $token
            ]
        ]);
        $result = $this->get('/api/logout');

        $this->assertEquals(json_encode($expected), (string)$this->_response->getBody());

        return null;
    }

    /**
     * tests the registration function of the API
     *
     * @return CakeResponse|null
     */
    public function testRegister()
    {
        $key = $this->key;
        $token = $this->token;
        //failed because there is no header
        $expected = json_decode('{
            "type": "fail",
            "code": 401,
            "message": "Unauthorize Access",
            "data": null
        }', true);

        $this->post('/api/register', [
            'firstname' => 'frenz',
            'lastname' => 'delgado',
            'mobile_number' => '09982947283',
            'email_address' => 'new@gmail.com',
            'password' => '111111',
            'retypePassword' => '111111'
        ]);

        $this->assertEquals(json_encode($expected), (string)$this->_response->getBody());
        // $this->assertResponseOk();

        $this->configRequest([
            'headers' => [
                'Accept' => 'application/json',
                'X-Api-Key' => $key
            ]
        ]);
        //fail because email is already in use
        $expected = json_decode('{
            "type": "fail",
            "code": 500,
            "message": "Error in registration",
            "data": null
        }', true);
        $this->post('/api/register', [
            'firstname' => 'frenz',
            'lastname' => 'delgado',
            'mobile_number' => '09982947283',
            'email_address' => 'new@gmail.com',
            'password' => '111111',
            'retypePassword' => '111111'
        ]);

        // $this->assertEquals(json_encode($expected), (string)$this->_response->getBody());

        return null;
    }
}
