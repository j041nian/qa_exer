<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * UsersFixture
 */
class UsersFixture extends TestFixture
{
    public $import = ['table' => 'users'];
    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer'],
        'profile_picture' => [
            'type' => 'string',
            'length' => 50,
            'null' => false
        ],
        'firstname' => [
            'type' => 'string',
            'length' => 50,
            'null' => false
        ],
        'lastname' => [
            'type' => 'string',
            'length' => 50,
            'null' => false
        ],
        'email_address' => [
            'type' => 'string',
            'length' => 60,
            'null' => false
        ],
        'password' => [
            'type' => 'string',
            'length' => 60,
            'null' => false
        ],
        'created' => 'datetime',
        'modified' => 'datetime',
        'is_confirmed' => [
            'type' => 'integer',
            'length' => 1
        ],
        'activation_id' => [
            'type' => 'string',
            'length' => 60,
            'null' => false
        ],
        'token' => [
            'type' => 'string',
            'length' => 50,
            'null' => true
        ],
        'token_expire' => 'datetime',
        'is_active' => [
            'type' => 'integer',
            'length' => 1
        ]
    ];

    // @codingStandardsIgnoreEnd
    /**
     * Init method
     *
     * @return void
     */
    public function init()
    {
        $this->records = [
            [
                'id' => 96,
                'profile_picture' => 'default.png',
                'firstname' => 'frenz',
                'lastname' => 'delgado',
                'email_address' => 'delgado.frenz@gmail.com',
                'mobile_number' => '09123892748',
                'created' => '2019-06-07 17:15:14',
                'modified' => '2019-07-03 14:29:54',
                'is_confirmed' => 1,
                'activation_id' => '1a6aa87424fc813d84736c6b32a78fe6',
                'token' => '3d94e4446c9389d133cf9064f7a8bc12',
                'token_expire' => '2019-07-04 14:29:54',
                'is_active' => 1
            ],
        ];
        parent::init();
    }
}
