<?php
use Migrations\AbstractMigration;

class FollowsMigration extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('follows');
        $table->addColumn('follower_id', 'integer', [
            'limit' => 11,
            'null' => false,
        ]);
        $table->addColumn('following_id', 'integer', [
            'limit' => 11,
            'null' => false
        ]);
        $table->addColumn('created', 'datetime', [
            'default' => null,
            'null' => false
        ]);
        $table->create();
    }
}
