<?php
use Migrations\AbstractMigration;

class PostsMigration extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('posts');
        $table->addColumn('poster_user_id', 'integer', [
            'limit' => 11,
            'null' => false,
        ]);
        $table->addColumn('retweet_user_id', 'integer', [
            'limit' => 11,
            'null' => true
        ]);
        $table->addColumn('content', 'string', [
            'limit' => 140,
            'null' => true
        ]);
        $table->addColumn('picture', 'string', [
            'limit' => 500,
            'null' => true
        ]);
        $table->addColumn('created', 'datetime', [
            'default' => null,
            'null' => false
        ]);
        $table->addColumn('modified', 'datetime', [
            'default' => null,
            'null' => false
        ]);
        $table->addColumn('deleted', 'datetime', [
            'default' => null,
            'null' => false
        ]);
        $table->create();
    }
}
