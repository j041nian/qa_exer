<?php
use Migrations\AbstractMigration;

class CommentsMigration extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('comments');
        $table->addColumn('user_id', 'integer', [
            'limit' => 11,
            'null' => false,
        ]);
        $table->addColumn('post_id', 'integer', [
            'limit' => 11,
            'null' => false
        ]);
        $table->addColumn('comment', 'string', [
            'limit' => 140,
            'null' => false
        ]);
        $table->addColumn('created', 'datetime', [
            'default' => null,
            'null' => false
        ]);
        $table->addColumn('modified', 'datetime', [
            'default' => null,
            'null' => false
        ]);
        $table->addColumn('deleted', 'datetime', [
            'default' => null,
            'null' => false
        ]);
        $table->create();
    }
}
