<?php
use Migrations\AbstractMigration;

class LikesMigration extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('likes');
        $table->addColumn('liker_user_id', 'integer', [
            'limit' => 11,
            'null' => false,
        ]);
        $table->addColumn('post_id', 'integer', [
            'limit' => 11,
            'null' => true
        ]);
        $table->addColumn('created', 'datetime', [
            'default' => null,
            'null' => false
        ]);
        $table->create();
    }
}
