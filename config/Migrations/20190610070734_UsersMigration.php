<?php
use Migrations\AbstractMigration;

class UsersMigration extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('users');
        $table->addColumn('profile_picture', 'string', [
            'limit' => 500,
            'null' => false,
        ]);
        $table->addColumn('firstname', 'string', [
            'limit' => 50,
            'null' => false
        ]);
        $table->addColumn('latname', 'string', [
            'limit' => 50,
            'null' => false
        ]);
        $table->addColumn('email_address', 'string', [
            'limit' => 60,
            'null' => false
        ]);
        $table->addColumn('password', 'string', [
            'limit' => 100,
            'null' => false
        ]);
        $table->addColumn('created', 'datetime', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('modified', 'datetime', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('is_confirmed', 'integer', [
            'limit' => 1,
            'null' => false
        ]);
        $table->addColumn('activation_id', 'string', [
            'limit' => 60,
            'null' => false
        ]);
        $table->addColumn('token', 'string', [
            'limit' => 50,
            'null' => true
        ]);
        $table->addColumn('token_expire', 'datetime', [
            'null' => true
        ]);
        $table->addColumn('is_active', 'integer', [
            'limit' => 1,
            'null' => false
        ]);
        $table->create();
    }
}
