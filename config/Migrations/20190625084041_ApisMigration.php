<?php
use Migrations\AbstractMigration;

class ApisMigration extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('apis');
        $table->addColumn('api_key', 'string', [
            'limit' => 50,
            'primary' => true,
            'null' => false,
        ]);
        $table->create();
    }
}
