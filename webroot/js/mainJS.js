function numbersOnly() {
    var keycode = event.which;
    if (!(event.shiftKey == false && (keycode == 46 || keycode == 8 || keycode == 37 || keycode == 39 || (keycode >= 48 && keycode <= 57)))) {
       event.preventDefault();
    }
}
function addComment(button) {
    var comment_id = 'commentContent' + button;
    var comment = document.getElementById(comment_id).value;
    var formUrl = '/comments/addComment/' + button;
    $.ajax({
        type: 'GET',
        url: formUrl,
        data: {commentContent : comment},
        success: function (data) {
            // alert(data);
            window.location.reload();
        }
   });
}
function editComment(button) {
    var id = button.replace('editComment', '');
    var comment_id = 'commentIdContent' + id;
    var comment = document.getElementById(comment_id).value;
    var formUrl = '/comments/editComment/' + id;
    $.ajax({
        type: 'GET',
        url: formUrl,
        data: {commentContent : comment},
        success: function (data) {
            window.location.reload();
        }
    });
}
function likePost(button) {
    var post_id = button.replace('like', '');
    var url = '/likes/like/' + post_id;
    var likes = document.getElementById('postLikeLabel' + post_id).textContent;
    $.ajax({
        type: 'GET',
        url: url,
        data: {post_id : post_id},
        success: function (data) {
            likes = likes == '' ? 1 : parseInt(likes) + 1;
            $('#like' + post_id).attr('onclick', 'unlikePost(this.id)');
            $('#likeIcon' + post_id).attr('class', 'fa fa-thumbs-up');
            document.getElementById('postLikeLabel' + post_id).textContent = likes;
       }
    });  
}
function unlikePost(button) {
    var post_id = button.replace('like', '');
    var url = '/likes/unlike/' + post_id;
    var likes = document.getElementById('postLikeLabel' + post_id).textContent;
    $.ajax({
        type: 'GET',
        url: url,
        data: {post_id : post_id},
        success: function (data) {
            likes = likes == 1 ? '' : parseInt(likes) - 1;
            $('#like' + post_id).attr('onclick', 'likePost(this.id)');
            $('#likeIcon' + post_id).attr('class', 'fa fa-thumbs-up stroke-transparent');
            document.getElementById('postLikeLabel' + post_id).textContent = likes;
        }
    });
}
function retweetPost(button) {
    var id = button.replace('retweet', '');
    var post_id = document.getElementById('retweetPostId' + id).value;
    var user_id = document.getElementById('retweetPosterUserId' + id).value;
    var content = document.getElementById('retweetContent' + id).value;
    var url = '/posts/retweet/' + user_id +'/' + post_id;
    $.ajax({
        type: 'GET',
        url: url,
        data: {
           retweet_user_id: user_id, 
           content: content,
           retweet_post_id: post_id,
        },
        success: function (data) {
            window.location.reload();
        }
    });
}
function unTweetPost(button) {
    var id = button.replace('retweet', '');
    var post_id = document.getElementById('retweetPostId' + id).value;
    var user_id = document.getElementById('retweetPosterUserId' + id).value;
    var url = '/posts/delete/' + user_id +'/' + post_id;
    $.ajax({
        type: 'GET',
        url: url,
        data: {retweet_user_id : user_id},
        success: function (data) {
            window.location.reload();
        }
    });
}
function deleteComment(button) {
    var isDelete = confirm('Are you sure you want to delete this comment?');
    if (isDelete) {
       var id = button.replace('deleteComment','');
       var url = '/comments/delete/' + id;
       $.ajax({
            type: 'GET',
            url: url,
            data: {comment_id : id},
            success: function (data) {
                window.location.reload();
            }
       });
   }
}
function deletePost(button) {
   var isDelete = confirm('Are you sure you want to delete this post?');
   if (isDelete) {
        var id = button.replace('deletePost', '');
        var post_id = document.getElementById('retweetPostId' + id).value;
        var user_id = document.getElementById('retweetPosterUserId' + id).value;
        var url = '/posts/delete/' + user_id +'/' + post_id;
        $.ajax({
            type: 'GET',
            url: url,
            data: {retweet_user_id : post_id},
            success: function (data) {
                window.location.reload();
            }
        });
   }
}

//FOR IMAGE
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#postImage').attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
}