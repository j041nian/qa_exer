<?php
// src/Model/Table/FollowsTable.php
namespace App\Model\Table;

use Cake\ORM\Table;

class FollowsTable extends Table
{
    /**
     * initializing the comments table table abd for configuring the table
     *
     * @param array $config the configuration to be made
     *
     * @return CakeResponse|null
     */
    public function initialize(array $config)
    {
        $this->addBehavior('Timestamp');
        $this->setTable('follows');
        $this->setPrimaryKey('id');

        $this->belongsTo('Follower', [
            'className' => 'Users',
            'foreignKey' => 'follower_id'
        ]);
        $this->belongsTo('Following', [
            'className' => 'Users',
            'foreignKey' => 'following_id'
        ]);

        return null;
    }
}
