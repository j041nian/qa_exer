<?php
// src/Model/Table/CommentsTable.php
namespace App\Model\Table;

use Cake\ORM\Table;

class CommentsTable extends Table
{
    /**
     * initializing the comments table table abd for configuring the table
     *
     * @param array $config the configuration to be made
     *
     * @return CakeResponse|null
     */
    public function initialize(array $config)
    {
        $this->addBehavior('Timestamp');
        $this->setTable('comments');
        $this->setPrimaryKey('id');

        return null;
    }
}
