<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Token Entity
 *
 * @property int $id
 * @property string $key
 * @property int $user_id
 * @property \Cake\I18n\FrozenTime $expiration
 * @property \Cake\I18n\FrozenTime $created
 * @property bool|null $deleted
 * @property \Cake\I18n\FrozenTime|null $deleted_date
 *
 * @property \App\Model\Entity\User $user
 */
class Token extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'key' => true,
        'user_id' => true,
        'expiration' => true,
        'created' => true,
        'deleted' => true,
        'deleted_date' => true,
        'user' => true
    ];
}
