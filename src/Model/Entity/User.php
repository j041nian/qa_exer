<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\Utility\Security;

/**
 * User Entity
 *
 * @property int $id
 * @property string|null $profile_picture
 * @property string $firstname
 * @property string $lastname
 * @property string $email_address
 * @property string $password
 * @property int|null $is_active
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Activation $activation
 * @property \App\Model\Entity\Comment[] $comments
 * @property \App\Model\Entity\Like[] $likes
 * @property \App\Model\Entity\Post[] $posts
 */
class User extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'profile_picture' => true,
        'firstname' => true,
        'lastname' => true,
        'email_address' => true,
        'password' => true,
        'activation' => true,
        'is_active' => true,
        'created' => true,
        'modified' => true,
        'comments' => true,
        'likes' => true,
        'posts' => true
    ];

    /**
     * for hashing password before saving using default password hasher
     *
     * @param string $password the actual password the user inputed
     * Fields that are excluded from JSON versions of the entity.
     *
     * @return string password the hashed password to be saved to the database
     */
    protected function _setPassword($password)
    {
        return Security::hash($password);
    }

    /**
     * Fields that are excluded from JSON versions of the entity.
     *
     * @var array
     */
    protected $_hidden = [
        'password'
    ];
}
