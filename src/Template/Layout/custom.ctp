<?php
    /**
     * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
     * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
     *
     * Licensed under The MIT License
     * For full copyright and license information, please see the LICENSE.txt
     * Redistributions of files must retain the above copyright notice.
     *
     * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
     * @link          https://cakephp.org CakePHP(tm) Project
     * @package       app.View.Layouts
     * @since         CakePHP(tm) v 0.10.0.1076
     * @license       https://opensource.org/licenses/mit-license.php MIT License
     */

    $cakeDescription = __d('cake_dev', 'CakePHP: the rapid development php framework');
    // $cakeVersion = __d('cake_dev', 'CakePHP %s', Configure::version());

    header('content-type: application/json');
    header('Access-Control-Allow-Origin: *');
    header("Cache-Control: no-cache, no-store, must-revalidate");
    header("Pragma: no-cache");
    header("Expires: 0");
?>
<!DOCTYPE html>
<html>
<head>
    <?php echo $this->Html->charset(); ?>
    <title>Microblog</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <?php
        echo $this->Html->meta('icon');
        echo $this->Html->css('custom');
        echo $this->Html->css('animate');
//        echo $this->Html->script('mainJS');

//        echo $this->fetch('meta');
        echo $this->fetch('css');
        echo $this->fetch('script');
    ?>
</head>
<body style="background-color: #c2c5c6">
    <div id="container">
        <?php echo $this->Flash->render(); ?>
        <div id="logo-container">
            <?php echo $this->Html->image("logo.png",array('class' => 'image-icon64'));?>
        </div>
        <?php echo $this->fetch('content'); ?>
    </div>
</body>
</html>
