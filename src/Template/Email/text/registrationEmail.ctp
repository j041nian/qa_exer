Welcome <strong><?= $name ?></strong>, <br>
You have successfully created an account in microblog. To confirm your account, please click <a href='<?= $url ?>'>here</a>.