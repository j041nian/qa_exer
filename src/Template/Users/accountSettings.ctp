<div class="col-lg-4 col-md-4 col-sm-2 col-xs-2"></div>
<div class="col-lg-4 col-md-5 col-sm-8 col-xs-9" style="background-color:white">
    <h2 class="text-center">Update Password</h2>
    <?= $this->Form->create('Users', ['enctype' => 'multipart/form-data']) ?>
    <div class="form-group input-group" style="width:100%">
        <?php
            echo $this->Form->control('newPassword', [
                'type' => 'password',
                'class' => 'form-control',
                'placeholder' => 'Password',
                'value' => '',
                'maxlength' => '16',
                'minlength' => '4'
            ]);
        ?>
    </div>
    <div class="form-group input-group" style="width:100%">
        <?php
            echo $this->Form->control('retypeNewPassword', [
                'type' => 'password',
                'label' => 'Retype Password',
                'class' => 'form-control',
                'placeholder' => 'Retype Password',
                'maxlength' => '16',
                'minlength' => '6'
            ]);
        ?>
    </div>
    <div class="form-group">
        <?php 
            echo $this->Form->submit(__('Update'), ['class'=>'btn btn-def btn-block']);
            echo $this->Form->end();
        ?>
    </div><br>&nbsp;
</div>