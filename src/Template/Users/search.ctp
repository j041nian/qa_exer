<div style="border:1px solid black; margin-top: -22px">
    <div class="col-lg-2 col-md-2 col-sm-1 col-xs-1"></div>
    <div class="col-lg-8 col-md-8 col-sm-10 col-xs-10 whiteBackground" style="min-height: 150px">
        <br>
        Results for: <strong><?= $keyword ?> </strong><br><br>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h5><?= count($allUsers) > 0 ? 'Users found: '.count($allUsers) : '' ?></h5>
                <?php
                    foreach ($allUsers as $user) {
                ?>
                <div class="col-lg-3 col-md-4 col-sm-3 col-xs-6 text-center">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <?php 
                            $followingName = ucwords($user->firstname. ' '.$user->lastname);
                            $profilePicture = $user->profile_picture;
                            //FOR DISPLAYING YOUR PROFILE PICTURE
                            echo $this->Html->link($this->Html->image('/webroot/img/uploads/images/'.$profilePicture, [
                                'width' => '120', 
                                'height' => '120', 
                                'class' => 'img-circle'
                            ]), [
                                'action' => 'view', $user->id, 
                                'controller' => 'users'
                            ], ['escape' => false]);
                        ?>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 followName">
                        <?php 
                            echo $this->Html->link(h($followingName), [
                                'action' => 'view', $user->id, 
                                'controller' => 'users'
                            ], ['escape' => false]);
                        ?>
                    </div><br>&nbsp;
                </div>
                <?php }?>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h5><?= count($posts) > 0 ? 'Posts found: '.count($posts) : '' ?></h5>
                <!-- FOR PRODUCING A ROW PER POST -->
                <?php
                    $count = 0;
                    foreach ($posts as $post) {
                        $count++
                ?>
                    <div class="row">
                        <!-- FOR POST CONTENT-->
                        <div class="col-lg-12 col-md-12 col-sm-10 col-xs-12"> 
                            <br>
                            <!-- FOR PROFILE PICTURE OF THE POSTER -->
                            <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">
                                <?php
                                    echo $this->Html->link($this->Html->image("/webroot/img/uploads/images/". $post->poster->profile_picture, [
                                            'width' => '45', 
                                            'height' => '45', 
                                            'class' => 'img-circle'
                                        ]), [
                                            'action' => 'view', $post->poster->id, 
                                            'controller' => 'users'
                                        ], ['escape' => false]
                                    );
                               ?>
                            </div>
                            <?php 
                                if ($currentUserId === $post->poster_user_id) { 
                            ?>
                            <div class="btn-group" role="group">
                                <button type="button" class="btn btn-link dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <?php
                                            echo $this->Html->link($this->Html->tag('i', '', ['class' => 'fa fa-edit']).' Edit', 
                                                [
                                                    'controller' => 'posts', 
                                                    'action' => 'edit', $post->id
                                                ], ['escape' => false]) ?>
                                    </li>
                                    <li>
                                        <?php 
                                            echo $this->Html->link($this->Html->tag('i', '', ['class' => 'fa fa-trash']).' Delete', [
                                                'controller' => 'posts', 
                                                'action' => 'delete', $post->id
                                            ], [
                                                'escape' => false, 
                                                'confirm' => 'Are you sure you want to delete this post?'
                                            ]) 
                                        ?>
                                    </li>
                                </ul>
                            </div>
                            <!-- FOR EDIT AND DELETE BUTTON IF YOU ARE THE POST AUTHOR-->
                            <?php } ?>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-8">
                                <?php 
                                    $fullname = $post->poster->firstname.' '.$post->poster->lastname;
                                    echo $this->Html->link(ucwords($fullname), [
                                        'action' => 'view', $post->poster->id, 
                                        'controller' => 'users'
                                    ]); 
                                    echo '<br>';
                                    //Checks if the post content was edited
                                    $timeCount = 0;
                                    foreach ($post->created as $created) {
                                        $created = $created;
                                        break;
                                    }
                                    $timeCount = 0;
                                    foreach ($post->modified as $modified) {
                                        $modified = $modified;
                                        break;
                                    }

                                    if ($created === $modified) {
                                        echo 'Created on '.date('F j, Y g:i A', strtotime($created));
                                    } else {
                                        echo 'Edited on '.date('F j, Y g:i A', strtotime($modified));
                                    }
                                ?>
                            </div>
                            <br><br>&nbsp;
                            <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                                <?php 
                                    //FOR RETWEET CONTENT
                                    if ($post->retweet_user_id !== null) {
                                ?>
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #d7e9ea"><br>
                                    <?php
                                        //CHECKS IF THE CONTENT IS DELETED
                                        if ($post->retweet->deleted !== null) {
                                            echo 'Content not available. <br> &nbsp';
                                        } else {
                                    ?>
                                    <div class="col-lg-1 col-md-1 col-sm-2 col-xs-1">
                                        <?php
                                            echo $this->Html->link($this->Html->image("../upload/images/". $post->retweet_author->profile_picture, [
                                                    'width' => '45', 
                                                    'height' => '45', 
                                                    'class' => 'img-circle'
                                                ]), [
                                                    'action' => 'view', $post->retweet_author->id, 
                                                    'controller' => 'users'
                                                ], ['escape' => false]) ?>
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-8 col-xs-10">
                                        <?php 
                                            $fullname = $post->retweet_author->firstname.' '.$post->retweet_author->lastname;
                                            echo $this->Html->link(h($fullname), [
                                                'action' => 'view', $post->retweet_author->id,
                                                'controller' => 'users'
                                            ]); 
                                            echo '<br>';
                                            //Checks if the retweet content was edited
                                            $timeCount = 0;
                                            foreach ($post->retweet->created as $created) {
                                                $created = $created;
                                                break;
                                            }
                                            $timeCount = 0;
                                            foreach ($post->retweet->modified as $modified) {
                                                $modified = $modified;
                                                break;
                                            }

                                            if ($created === $modified) {
                                                echo 'Created on '.date('F j, Y g:i A', strtotime($created));
                                            } else {
                                                echo 'Edited on '.date('F j, Y g:i A', strtotime($modified));
                                            }
                                        ?>
                                    </div><br>
                                    <div class='col-lg-10 col-md-10'>
                                        <br>
                                        <?php
                                            //FOR POST CONTENT
                                            echo h($post['Retweet']['content']);
                                        ?>
                                        <div style="margin-left: 40%;">
                                            <?php 
                                                echo $this->Html->image('../upload/images/'.$post['Retweet']['picture'], [
                                                    'width' => '200', 
                                                    'height' => '200', 
                                                    'class' => 'img-responsive'
                                                ]) ?>
                                            <br>&nbsp;
                                        </div>
                                    </div>
                                    <?php } ?>
                                </div>
                                <?php
                                    }
                                    //END OF RETWEET CONTENT
                                ?>
                                <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">
                                </div>
                                <div class='col-lg-10 col-md-10'>
                                    <?php
                                        //FOR POST CONTENT
                                        echo h($post->content);
                                    ?>
                                    <div style="margin-left: 40%;">
                                        <?php 
                                            echo $this->Html->image('../upload/images/'.$post->picture, [
                                                'width' => '200', 
                                                'height' => '200', 
                                                'class' => 'img-responsive'
                                            ]) 
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-10 col-xs-12"> 
                            <br>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <?php
                                    //FOR LIKE BUTTON
                                    $likerCount = count($post->likes);
                                    $likeClass = 'stroke-transparent';
                                    $goTo = 'likePost(this.id)';
                                    $title = 'Click to like this post';
                                    $userId = '0';
                                    $actionId = $post->id;
                                    foreach ($post->likes as $like) {
                                        if ($like['liker_user_id'] === $currentUserId) {
                                            $likeClass = '';
                                            $goTo = 'unlikePost(this.id)';
                                            $title = 'Click to unlike this post';
                                            // $actionId = $like['like_id'];
                                            $userId = $like['liker_user_id'];
                                        }
                                    }
                                ?>
                                    <label id="postLikeLabel<?= $post->id ?>"><?= $likerCount > 0 ? $likerCount : '' ?></label>
                                <?php
                                    // echo  $likerCount > 0 ? $likerCount : '';
                                    echo $this->Form->button('<i class="fa fa-thumbs-up '.$likeClass.'" id="likeIcon'.$post->id.'"></i> Like', [
                                        'type' => 'button',
                                        'title' => $title,
                                        'id' => 'like'.$actionId,
                                        'class' => 'btn btn-link',
                                        'onclick' => $goTo
                                    ]);
                                    //FOR RETWEET BUTTON
                                $retweetCount = count($post->retweeters);
                                echo $retweetCount > 0 ? $retweetCount : '';
                                $retweetClass = 'stroke-transparent';
                                $title = 'Click to retweet this post';
                                $goTo = 'retweetPost(this.id)';
                                $text = 'Retweet';
                                if ($post->retweet_user_id !== null && $post->poster_user_id === $currentUserId) {
                                    $retweetUserId = $post->retweet_user_id;
                                    $retweetPostId = $post->retweet_post_id;
                                    if ($post->poster->id === $currentUserId) {
                                        $retweetClass = '';
                                        $title = 'Click to untweet this post';
                                        $text = 'Untweet';
                                        $retweetUserId =  $post->id;
                                        $retweetPostId = $post->id;
                                        $goTo = 'unTweetPost(this.id)';

                                        //FOR UNTWEET BUTTON
                                        echo $this->Form->button('<i class="fa fa-retweet '.$retweetClass.'"></i> '.$text, [
                                            'type' => 'button',
                                            'title' => $title,
                                            'id' => 'retweet'.$post->id,
                                            'class' => 'btn btn-link',
                                            'onclick' => $goTo
                                        ]);
                                    }
                                } else {
                                    if ($post->retweet_user_id === null) {
                                        $retweetUserId =  $post->poster_user_id;
                                        $retweetPostId = $post->id;
                                        $retweetProfilePicture = $post->poster->profile_picture;
                                        $retweetName = $post->poster->firstname.' '.$post->poster->lastname;
                                        $retweetContent = $post->content;
                                        $retweetPicture = $post->picture;
                                    } else {
                                        $retweetUserId =  $post->retweet_author->id;
                                        $retweetPostId = $post->retweet->id;
                                        $retweetProfilePicture = $post->retweet_author->profile_picture;
                                        $retweetName = $post->retweet_author->firstname.' '.$post->retweet_author->lastname;
                                        $retweetContent = $post->retweet->content;
                                        $retweetPicture = $post->retweet->picture;
                                    }

                                    //FOR TWEET BUTTON
                                    echo $this->Form->button('<span class="fa fa-retweet '.$retweetClass.'"></span> '.$text, [
                                        'type' => 'button',
                                        'title' => $title,
                                        'data-toggle' => 'modal',
                                        'data-target' => '#retweetModal'.$post->id,
                                        'id' => 'retweet'.$post->id,
                                        'class' => 'btn btn-link',
                                    ]);
                                ?>
                                <!-- RETWEET MODAL -->
                                <div class="modal fade" id="retweetModal<?= $post->id ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                                <h4 class="modal-title text-center" id="exampleModalLabel">Retweet Post </h4>
                                            </div>
                                            <div class="modal-body">
                                                <!-- RETWEET CONTENT -->
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <!-- FOR CREATING A RETWEET COMMENT -->
                                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-3">
                                                        <br>
                                                        <?php 
                                                            echo $this->Html->link($this->Html->image('/webroot/img/uploads/images/'.$myProfilePicture,
                                                            [
                                                                'width' => '45', 
                                                                'height' => '45', 
                                                                'style' => '',
                                                                'class' => 'img-circle'
                                                            ]),
                                                            '#', ['escape' => false]);
                                                       ?>
                                                    </div>
                                                    <div class="col-lg-11 col-md-11 col-sm-11 col-xs-9">
                                                        <?php
                                                            echo $this->Form->control('retweetContent'.$post->id,
                                                            [
                                                                'type' => 'textarea',
                                                                'class' => 'form-control',
                                                                'placeholder' => 'Write your thoughts here',
                                                                'id' => 'retweetContent'.$post->id,
                                                                'maxlength' => '140',
                                                                'height' => '100px',
                                                                'rows' => 3,
                                                                'resize' => 'none',
                                                                'label' => false
                                                            ]
                                                        );
                                                        ?>
                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #d7e9ea"><br>
                                                            <?php
                                                                //CHECKS IF THE CONTENT IS DELETED
                                                                if ($post->deleted !== null) {
                                                                    echo 'Content not available. <br> &nbsp';
                                                                } else {
                                                            ?>
                                                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-3">
                                                                <?php
                                                                    echo $this->Html->link($this->Html->image("/webroot/img/uploads/images/". $retweetProfilePicture, [
                                                                        'width' => '45', 
                                                                        'height' => '45', 
                                                                        'class' => 'img-circle'
                                                                    ]), [
                                                                        'action' => 'view', $post->poster_user_id , 
                                                                        'controller' => 'users'
                                                                    ], [
                                                                        'escape' => false
                                                                    ]) 
                                                                ?>
                                                            </div>
                                                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-9">
                                                            <?php 
                                                                echo $this->Html->link($retweetName, ['action' => 'view', $post->retweet_author_id , 'controller' => 'users']); 
                                                                echo '<br>';
                                                                //Checks if the retweet content was edited
                                                                $timeCount = 0;
                                                                foreach ($post->created as $created) {
                                                                    $created = $created;
                                                                    break;
                                                                }
                                                                $timeCount = 0;
                                                                foreach ($post->modified as $modified) {
                                                                    $modified = $modified;
                                                                    break;
                                                                }

                                                                if ($created === $modified) {
                                                                    echo 'Created on '.date('F j, Y g:i A', strtotime($created));
                                                                } else {
                                                                    echo 'Edited on '.date('F j, Y g:i A', strtotime($modified));
                                                                }
                                                            ?>
                                                        </div><br>
                                                        <div class='col-lg-10 col-md-10 col-'>
                                                            <br>
                                                            <?php
                                                                //FOR RETWEET POST CONTENT
                                                                echo h($retweetContent);
                                                            ?>
                                                            <div style="margin-left: 40%;">
                                                                <?php echo $this->Html->image('/webroot/upload/images/'.$retweetPicture,
                                                                ['width' => '200', 'height' => '200', 'class' => 'img-responsive']) ?>
                                                                <br>&nbsp;
                                                            </div>
                                                        </div>
                                                            <?php } ?>
                                                       </div>
                                                        <!-- END OF CREATING YOUR RETWEET CONTENT -->
                                                    </div>
                                                </div>
                                                <!-- END OF RETWEET CONTENT -->&nbsp;
                                            </div>
                                            <div class="modal-footer">
                                                <?php 
                                                    echo $this->Form->button('<i class="fa fa-retweet '.$retweetClass.'"></i> '.$text, [
                                                            'type' => 'button',
                                                            'title' => $title,
                                                            'id' => 'retweet'.$post->id,
                                                            'class' => 'btn btn-link',
                                                            'onclick' => $goTo
                                                        ]
                                                    );
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- END OF EDIT MODAL -->
                                <?php
                                }
                                echo $this->Form->hidden('retweetPostId'.$post->id, [
                                    'value' => $retweetPostId,
                                    'id' => 'retweetPostId'.$retweetPostId
                                ]);
                                echo $this->Form->hidden('retweetPosterUserId'.$post->id, [
                                    'value' => $retweetUserId,
                                    'id' => 'retweetPosterUserId'.$post->id
                                ]);
                            ?>
                            <hr style="height: 1px; background-color: black"> 
                    </div>
                    <hr style="height: 2px; background-color: black">
                <?php } ?>
                <!--END OF EDITED CONTENTES 5/2/2019-->
                <!-- END OF PRODUCING ROW-->
            </div>
        </div>
        <br>
    </div>
</div>