<?php include_once 'profileTemplate.ctp' ?>
<br>
    <!--FOR TABS-->
    <ul class="nav nav-tabs text-center" id='profileCategotyTab'>
        <li class="active">
            <a class="navTab" data-toggle="tab" href="#posts">
                <?= count($posts) ?><br>
                Posts
            </a>
        </li>
        <li>
            <a class="navTab" data-toggle="tab" href="#following">
                <?= count($followings) ?><br>
                Following
            </a>
        </li>
        <li>
            <a class="navTab" data-toggle="tab" href="#followers">
                <?= count($followers) ?><br>
                Followers
            </a>
        </li>
    </ul>
    <!--FOR TAB CONTENTS-->
    <div class="tab-content">
        <div id="posts" class="tab-pane fade in active">
            <?php include_once 'myposts.ctp' ?>
        </div>
        <div id="following" class="tab-pane fade">
            <br>
            <?php include_once 'following.ctp' ?>
        </div>
        <div id="followers" class="tab-pane fade">
            <br>
            <?php include_once 'follower.ctp' ?>
        </div>
    </div>
</div>
<script type="text/javascript">
    $('#profileCategotyTab a').click(function(e) {
        e.preventDefault();
        $(this).tab('show');
    });
    $("ul.nav-tabs > li > a").on("shown.bs.tab", function(e) {
        var id = $(e.target).attr("href").substr(1);
        window.location.hash = id;
    });
    var hash = window.location.hash;
    $('#profileCategotyTab a[href = "' + hash + '"]').tab('show');
</script>