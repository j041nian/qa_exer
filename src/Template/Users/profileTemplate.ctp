<div class="col-lg-1 col-md-1 col-sm-12 col-xs-12 text-center"></div>
<div class="col-lg-2 col-md-3 col-sm-12 col-xs-12 text-center whiteBackground">
    <br>
    <?php
        //FOR YOUR PROFILE PICTURE
        //FOR DISPLAYING YOUR PROFILE PICTURE
        echo $this->Html->image('/webroot/img/uploads/images/'.$myProfilePicture, [
            'width' => '175', 
            'height' => '175', 
            'class' => 'img-circle'
        ]);
   ?><br>
   <!-- FOR YOUR NAME-->
   <label id="profileName"><?= h($fullname) ?></label><br>
   <?php
        echo $this->Html->link('Edit Profile', [
            'controller' => 'users', 
            'action' => 'edit'
        ], [
            'class' => 'btn',
            'id'    => 'profileButton',
        ]);
    ?>
   <br>&nbsp;
</div>
<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1" id='customProfile'>&nbsp;</div>
<div class="col-lg-8 col-md-7 col-sm-12 col-xs-12 whiteBackground">
    <!-- PAGE CONTENT -->