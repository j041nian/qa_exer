<div class="col-lg-4 col-md-4 col-sm-2 col-xs-2"></div>
<div class="col-lg-4 col-md-5 col-sm-8 col-xs-9" style="background-color:white">
    <h2 class="text-center">Edit Profile</h2>
    <?php
        echo $this->Form->create('Users', ['type' => 'file']) 
    ?>
    <div class="form-group input-group" style="width:100%">
        <?php
            echo $this->Form->control('', [
                'type' => 'file',
                'name' => 'picture',
                'class' => 'file form-control'
            ]);
        ?>
    </div>
    <div class="form-group input-group" style="width:100%">
        <?php
            echo $this->Form->control('firstname', [
                'type' => 'text',
                'class' => 'form-control',
                'value' => $user->firstname,
                'placeholder' => 'Firstname'
            ]);
        ?>
    </div>
    <div class="form-group input-group" style="width:100%">
        <?php
            echo $this->Form->control('lastname', [
                'type' => 'text',
                'class' => 'form-control',
                'value' => $user->lastname,
                'placeholder' => 'Lastname'
            ]);
        ?>
    </div>
    <div class="form-group input-group" style="width:100%">
        <?php
            echo $this->Form->control('mobile_number', [
                'type' => 'text',
                'class' => 'form-control',
                'placeholder' => 'Mobile Number',
                'maxlength' => '11',
                'minlength' => '11',
                'value' => $user->mobile_number,
                'onkeypress' => 'numbersOnly()'
            ]);
        ?>                   
    </div>
    <!-- <div class="form-group input-group" style="width:100%">
        <?php
            echo $this->Form->control('email_address', [
                'type' => 'email',
                'class' => 'form-control',
                'value' => $user->email_address,
                'placeholder' => 'Email Address'
            ]);
        ?>                   
    </div> -->
    <div class="form-group">
        <?php 
            echo $this->Form->submit(__('Update'),array('class'=>'btn btn-def btn-block'));
            echo $this->Form->end();
        ?>
    </div><br>&nbsp;
</div>