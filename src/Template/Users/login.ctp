<div>
    <!-- Modal -->
    <div id="ajaxModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="loader"></div>
        </div>
    </div>

    <p>&nbsp;</p>
    <p>&nbsp;</p>
    <div class="col-lg-4 col-md-4 col-sm-2 col-xs-2"></div>
    <div class="col-lg-4 col-md-5 col-sm-8 col-xs-9" style="background-color:white">
        <h1 class="text-center">Login</h1>
        <?php
            echo $this->Form->create('User');
        ?>
        <div class="form-group input-group">
            <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
            <?php
                echo $this->Form->controller('email_address', [
                    'type' => 'text',
                    'class' => 'form-control',
                    'label' => false,
                    'placeholder' => 'Email Address'
                ]);
            ?>
        </div>
        <div class="form-group input-group">
            <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
            <?php
                echo $this->Form->controller('password', [
                    'type' => 'password',
                    'class' => 'form-control',
                    'label' => false,
                    'placeholder' => 'Password'
                ]);
            ?>
        </div>
        <div class="form-group">
            <?= $this->Form->submit(__('Login'), ['class'=>'btn btn-custom btn-block btn-red']) ?>
        </div>
        <div class="form-group text-center">
            Register
            <?php
                echo $this->Html->link('here', [
                    'controller' => 'users',
                    'action' => 'register'
                ]);
            ?>
        </div>
    </div>
</div>

<script>
$("input[type=submit]").click(function(e) {
    var id = this.id;
    $("#ajaxModal").modal({
        backdrop: 'static',
        keyboard: false,
        show: true
    });
});
</script>
