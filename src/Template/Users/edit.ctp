<?php include_once 'profileTemplate.ctp' ?>
    <br>
    <!--FOR TABS-->
    <ul class="nav nav-tabs text-center" id='editTab'>
        <li class="active">
            <a class="navTab" data-toggle="tab" href="#personalInformation">
                <span class="glyphicon glyphicon-user"></span><br>
                Personal Information
            </a>
        </li>
        <li>
            <a class="navTab" data-toggle="tab" href="#accountSettings">
                <i class="glyphicon glyphicon-cog"></i><br>
                Account Settings
            </a>
        </li>
    </ul>
    <!--FOR TAB CONTENTS-->
    <div class="tab-content">
        <div id="personalInformation" class="tab-pane fade in active">
            <?php //print_r($users) ?>
            <?php include_once 'personalInformation.ctp' ?>
        </div>
        <div id="accountSettings" class="tab-pane fade">
            <br>
            <?php include_once 'accountSettings.ctp' ?>
        </div>
    </div>
</div>
<script type="text/javascript">
    $('#editTab a').click(function(e) {
        e.preventDefault();
        $(this).tab('show');
    });
    
    $("ul.nav-tabs > li > a").on("shown.bs.tab", function(e) {
        var id = $(e.target).attr("href").substr(1);
        window.location.hash = id;
    });
    
    var hash = window.location.hash;
    $('#editTab a[href = "' + hash + '"]').tab('show');
</script>