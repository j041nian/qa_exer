<!-- Modal -->
<div id="ajaxModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="loader"></div>
    </div>
</div>

<div class="register-form">
    <div class="col-lg-4 col-md-4 col-sm-2 col-xs-2"></div>
    <div class="col-lg-4 col-md-5 col-sm-8 col-xs-9" style="background-color:white">
        <h1 class="text-center">Registration</h1>
        <?php echo $this->Form->create($user) ?>
        <div class="form-group input-group" style="width:100%">
            <?php
                echo $this->Form->control('firstname', [
                    'type' => 'text',
                    'class' => 'form-control',
                    'placeholder' => 'Firstname',
                    'required' => false
                ]);
            ?>
        </div>
        <div class="form-group input-group" style="width:100%">
            <?php
                echo $this->Form->control('lastname', [
                    'type' => 'text',
                    'class' => 'form-control',
                    'placeholder' => 'Lastname',
                    'required' => false
                ]);
            ?>
        </div>
        <div class="form-group input-group" style="width:100%">
            <?php
                echo $this->Form->control('mobile_number', [
                    'type' => 'text',
                    'class' => 'form-control',
                    'placeholder' => 'Mobile Number',
                    'required' => false
                ]);
            ?>
        </div>
        <div class="form-group input-group" style="width:100%">
            <?php
                echo $this->Form->control('email_address', [
                    'type' => 'text',
                    'class' => 'form-control',
                    'placeholder' => 'Email Address',
                    'required' => false
                ]);
            ?>
        </div>
        <div class="form-group input-group" style="width:100%">
            <?php
                echo $this->Form->control('password', [
                    'type' => 'password',
                    'class' => 'form-control',
                    'placeholder' => 'Password',
                    'required' => false
                ]);
            ?>
        </div>
        <div class="form-group input-group" style="width:100%">
            <?php
                echo $this->Form->control('retype_password', [
                    'type' => 'password',
                    'class' => 'form-control',
                    'placeholder' => 'Retype Password',
                    'required' => false
                ]);
            ?>
        </div>
        <div class="form-group">
            <?php echo $this->Form->submit(__('Register'), ['class'=>'btn btn-custom btn-block btn-red']) ?>
        </div>
        <div class="form-group text-center">
            Go back to 
            <?php
                echo $this->Html->link('login', [
                    'controller' => 'users', 
                    'action' => 'login'
                ]);
                echo $this->Form->end() 
            ?>
        </div>
    </div>
</div>

<script>
$("input[type=submit]").click(function(e) {
    var id = this.id;
    $("#ajaxModal").modal({
        backdrop: 'static',
        keyboard: false,
        show: true
    });
});
</script>
