<?php
    foreach ($followings as $following) {
?>
    <div class="col-lg-3 col-md-4 col-sm-3 col-xs-6 text-center">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <?php 
                $followingName = ucwords($following->following['firstname']. ' '.$following->following['lastname']);
                $profilePicture = $following->following['profile_picture'];
                //FOR DISPLAYING YOUR PROFILE PICTURE
                echo $this->Html->link($this->Html->image('/webroot/img/uploads/images/'.$profilePicture,
                [
                    'width' => '120', 
                    'height' => '120', 
                    'class' => 'img-circle'
                ]), [
                    'action' => 'view', $following['following_id'], 
                    'controller' => 'users'
                ], ['escape' => false]);
            ?>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 followName">
            <?php 
                echo $this->Html->link(h($followingName), [
                    'action' => 'view', $following['following_id'],
                    'controller' => 'users'
                ], ['escape' => false]).'<br>&nbsp;';
                if ($currentUserId === $following['follower_id']) {
                    echo $this->Html->link(__('Unfollow'), [
                        'controller' => 'follows', 
                        'action' => 'unfollow', $following['id'], $currentUserId
                    ], ['class' => 'btn unfollowProfile', 'id' => 'unfollow', 'confirm'=>'Are you sure you want to unfollow this user?']);
                }
            ?>
        </div><br>&nbsp;
    </div>
<?php 
    }
    if (count($followings) === 0) {
        echo '<h3> No following found.</h3>';
    }
?>