<?php
    $fullname = ucwords($user['firstname'].' '.$user['lastname']);
    $profilePicture = $user['profile_picture'];
?>

<div class="col-lg-1 col-md-1 col-sm-12 col-xs-12 text-center"></div>
<div class="col-lg-2 col-md-3 col-sm-12 col-xs-12 text-center whiteBackground">
    <br>
    <?php
        //FOR DISPLAYING YOUR PROFILE PICTURE
        echo $this->Html->image('/webroot/img/uploads/images/'.$profilePicture, [
            'width' => '175', 
            'height' => '175', 
            'class' => 'img-circle'
        ]);
   ?><br>
   <!-- FOR YOUR NAME-->
   <label id="profileName"><?= h($fullname) ?></label><br>
   <?php 
        $isFollowing = false;
        // print_r($user);
        foreach ($followers as $follower) {
            if ($follower['follower_id'] === $currentUserId) {
                $isFollowing = true;
                $followId = $follower['id'];
                break;
            }
        }
        if ($isFollowing) {
            echo $this->Html->link(__('Unfollow'), [
                'controller' => 'follows', 
                'action' => 'unfollow', $followId, $user['id']
            ], [
                'class' => 'btn unfollow', 
                'id' => 'unfollow', 
                'confirm'=>'Are you sure you want to unfollow this user?'
            ]);
        } else {
            echo $this->Html->link(__('Follow'), [
                'controller' => 'follows', 
                'action' => 'follow', $user['id']
            ], [
                'class' => 'btn follow', 
                'id' => 'follow'
            ]);
        }
        // print_r($user)
    ?>
   <br>&nbsp;
</div>
<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1" id='customProfile'>&nbsp;</div>
<div class="col-lg-8 col-md-7 col-sm-12 col-xs-12 whiteBackground">
    <br>
    <!--FOR TABS-->
    <ul class="nav nav-tabs text-center" id='viewProfileTab'>
        <li class="active">
            <a class="navTab" data-toggle="tab" href="#posts">
                <?= count($posts) ?><br>
                Posts
            </a>
        </li>
        <li>
            <a class="navTab" data-toggle="tab" href="#following">
                <?= count($followings) ?><br>
                Following
            </a>
        </li>
        <li>
            <a class="navTab" data-toggle="tab" href="#followers">
                <?= count($followers) ?><br>
                Followers
            </a>
        </li>
    </ul>
    <!--FOR TAB CONTENTS-->
    <div class="tab-content">
        <div id="posts" class="tab-pane fade in active">
            <?php //print_r($users) ?>
            <?php include_once 'myposts.ctp' ?>
        </div>
        <div id="following" class="tab-pane fade">
            <br>
            <?php include_once 'following.ctp' ?>
        </div>
        <div id="followers" class="tab-pane fade">
            <br>
            <?php include_once 'follower.ctp' ?>
        </div>
    </div>
</div>
<script type="text/javascript">
    $('#viewProfileTab a').click(function(e) {
        e.preventDefault();
        $(this).tab('show');
    });
    
    $("ul.nav-tabs > li > a").on("shown.bs.tab", function(e) {
        var id = $(e.target).attr("href").substr(1);
        window.location.hash = id;
    });
    
    var hash = window.location.hash;
    $('#viewProfileTab a[href = "' + hash + '"]').tab('show');
</script>