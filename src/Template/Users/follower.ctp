<?php foreach ($followers as $follower) { ?>
    <div class="col-lg-3 col-md-4 col-sm-3 col-xs-6 text-center">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <?php 
                $followerName = ucwords($follower->follower['firstname']. ' '.$follower->follower['lastname']);
                $profilePicture = $follower->follower['profile_picture'];
                //FOR DISPLAYING YOUR PROFILE PICTURE
                echo $this->Html->link($this->Html->image('/webroot/img/uploads/images/'.$profilePicture, [
                    'width' => '120', 
                    'height' => '120', 
                    'class' => 'img-circle'
                ]), [
                    'action' => 'view', $follower->follower_id, 
                    'controller' => 'users'
                ], ['escape' => false]);
            ?>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 followName">
            <?php
                echo $this->Html->link(h($followerName), [
                    'action' => 'view', $follower->follower_id, 
                    'controller' => 'users'
                ], ['escape' => false]) 
            ?>
        </div><br>&nbsp;
    </div>
<?php 
    }
    if (count($followers) === 0) {
        echo '<h3> No Followers found.</h3>';
    }
?>