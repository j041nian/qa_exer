<?php $session = $this->Session->read('Auth');?>
<?php foreach ($posts as $index => $post): ?>
    <?php if ($index == 0): ?>
        <?= $this->element('postbox', ['post' => $post]);?>
    <?php else: ?>
        <?= $this->element('postdefault', ['post' => $post]);?>
    <?php endif; ?>
<?php endforeach; ?>

<script>
$(document).ready(function() {
    $('.post-rec:odd').addClass('pull-left');
    $('.post-rec:even').addClass('pull-right');
    $("<div class='clearfix'></div>").insertBefore(".post-rec:odd");

    $('[data-toggle="popover"]').popover({
        container: 'body',
        html: true
    });

    $('textarea').css('height', '32px');

    $(document).on('click', function(e) {
        $('[data-toggle="popover"],[data-original-title]').each(function() {
            if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
                (($(this).popover('hide').data('bs.popover') || {}).inState || {}).click = false // fix for BS 3.3.6
            }

        });
    });
});

$(document).on('click', '.post-content', function(e) {
    if ($('#ajaxModal').is(':visible') == false) {
        var target = $(e.target);
        if (!target.is('a') &&
            !target.parent('div').is('.panel-body') &&
            !target.parent('div').is('.panel-heading') &&
            !target.is('img') &&
            !target.is('.panel-heading') &&
            !target.is('textarea') &&
            !target.is('.panel-body')) {
            thread(target.attr('id'));
        }
    }
});
</script>
