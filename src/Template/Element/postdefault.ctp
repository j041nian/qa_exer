<?php $session = $this->Session->read('Auth');?>
<div class="post-rec animated bounceInRight" style="width: 49%">
    <div class="post-header">
        <span class="profile-name pull-left">
            <?php
                echo $this->Html->link($post['user']['firstname']." ".$post['user']['lastname'],
                    array(
                        'controller'=>'users',
                        'action'=> 'accounts',$post['user']['firstname']
                    )
                );
            ?>

            <i class="profile-user">@<?php echo $post['user']['firstname'];?>
                <?php echo date('M d',strtotime($post['created'])); ?>
            </i>
        </span>

            <span class="side-hover">
            <div class="front">
                <span class="pull-right"><?php echo $this->Post->aging($post['created']); ?></span>
        </div>

        <?php if ($session['User']['id'] == $post['user']['id']) :?>
        <div class="back">
            <span class="pull-right">
                <a class="" data-toggle="popover" title="Options" data-placement="bottom"
                    data-content="<a href='#' onclick='delete_post(<?php echo $post['id'];?>)'>Delete</a> <br>
                    <a href='#' onclick='edit(<?php echo $post['id'];?>)'>Edit</a>
                    ">
                   <i class="fa fa-ellipsis-h fa-2x"></i>
                </a>
            </span>
        </div>

        <?php endif;?>
        </span>
    </div>

    <div class="profile-rec">
        <div class="profile-bubble">
            <?php //echo $this->Html->image($post['user']['firstname']."/".$post['Photo']['photo_name'],array('class' => 'profile-circle'));?>
        </div>
    </div>

    <div class="clearfix"></div>
    <div id="<?php echo $post['id']; ?>" class="col-md-11 post-content">
    <?php echo $this->Post->hashtag($post['content']);?>
        <div class="clearfix"></div>
        <?php if (!empty($post['Picture'])) :?>
        <?php echo $this->Html->image($post['user']['firstname']."/".$post['Picture']['photo_name'],array('class'=>'picture_content')); ?>
        <?php endif; ?>

        <?php if (!is_null($post['parent_id'])) :?>
            <?php if (!is_null($post['Parent']['id'])) :?>
            <div class="post-shared">
                <div class="panel panel-default">
                    <div class="panel-heading" style="font-size:8pt">
                        <?php echo "posted by: ".$parentUser;?>
                    </div>
                    <div class="panel-body">
                        <span class="fa fa-quote-left pull-left"></span> <b style="font-size: 16pt"><?php echo $post['Parent']['content']; ?></b> <span class="fa fa-quote-right"></span>
                        <?php if (!empty($post['Parent']['photo_id']) && !is_null($post['Parent']['photo_id'])) :?>
                        <?php echo $this->Html->image($folder."/".$post['SharedPhoto']['photo_name'],array('class'=>'picture_content')); ?>
                        <?php endif;?>
                    </div>
                </div>
            </div>
            <?php else:?>
            <span class="help-block" style="font-size:8pt"><i class="fa fa-warning"></i> Shared Content Not Available</span>
            <?php endif;?>
        <?php endif;?>
    </div>

    <div class="clearfix"></div>
    <div class="post-footer">
        <?php
            echo $this->Form->textarea($post['id'], array(
                'label' => false,
                'placeholder' => 'Comment',
                'onkeyup' => 'textAreaAdjust(this),comment()',
                'class' => 'form-control comment',
                'maxlength' => 140,
            ));
        ?>

        <span class="footer-btn">
        <a class="btn btn-xs <?php //echo $unlike;?>" onclick="likePost(<?php echo $post['id']; ?>,this)"><?php //echo count($post['Upvote']);?> <i class="fa fa-heart-o"></i> Like</a> &nbsp;
        <a class="btn btn-xs" onclick="share(<?php echo $post['id']; ?>)"><?php //echo count($post['Share']); ?> <i class="fa fa-retweet"></i> Share</a>
    </span>

    </div>
</div>
