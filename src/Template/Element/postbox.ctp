<?php $session = $this->Session->read('Auth');?>
<div class="post-rec animated bounceInRight" style="width: 100%">
    <div class="post-header">
        <span class="profile-name pull-left">
            <?php
                echo $this->Html->link($post['user']['firstname']." ".$post['user']['lastname'],
                    ['controller'=>'users', 'action'=> 'accounts',$post['user']['firstname']]
                );
            ?>
            <i class="profile-user">
            <?php echo date('M d',strtotime($post['created'])); ?>
            </i>
        </span>

        <span class="side-hover">
            <div class="front">
                <span class="pull-right">
                    <?php echo $this->Post->aging($post['created']); ?>
                </span>
            </div>
            <div class="back">
                <?php
                    if ($session['User']['id'] == $post['user']['id']) :
                ?>
                <span class="pull-right">
                    <a class="" data-toggle="popover" title="Options" data-placement="bottom"
                      data-content="<a class='btn btn-xs btn-link' onclick='delete_post(<?php echo $post['id'];?>)'>Delete</a> <br>
                                    <a class='btn btn-xs btn-link' onclick='edit(<?php echo $post['id'];?>)'>Edit</a>
                                    ">
                       <i class="fa fa-ellipsis-h fa-2x"></i>
                    </a>
                </span>
                <?php else:?>
                <span class="pull-right">
                    <?php echo $this->Post->aging($post['created']); ?>
                </span>
                <?php endif;?>
            </div>
        </span>
    </div>

    <div class="profile-rec">
        <div class="profile-bubble">
            <?php
                echo $this->Html->image('user.png', ['class' => 'profile-circle']);
            ?>
        </div>
    </div>

    <div class="clearfix"></div>
    <div id="<?php echo $post['id']; ?>" class="col-md-11 post-content">
        <?php echo $this->Post->hashtag($post['content']);?>

        <div class="clearfix"></div>
        <?php if (!empty($post['Picture'])) :?>
        <?php
            echo $this->Html->image($post['user']['username']."/".$post['Picture']['photo_name'],
                  ['class'=>'picture_content']);
        ?>
        <?php endif; ?>
        <?php if (!is_null($post['parent_id'])) :?>
        <?php if (!is_null($post['Parent']['id'])) :?>
        <div class="post-shared">
            <div class="panel panel-default">
                <div class="panel-heading" style="font-size:8pt">
                    <?php echo "posted by: ".$parentUser;?>
                </div>
                <div class="panel-body">
                    <span class="fa fa-quote-left pull-left"></span> <b style="font-size: 16pt"><?php echo $this->Post->hashtag($post['Parent']['content']); ?></b> <span class="fa fa-quote-right"></span>
                    <?php if (!empty($post['SharedPhoto']['photo_name']) && !is_null($post['SharedPhoto']['photo_name'])) :?>
                    <?php
                        echo $this->Html->image($folder."/".$post['SharedPhoto']['photo_name'],
                            ['class'=>'picture_content']);
                    ?>
                    <?php endif;?>
                </div>
            </div>
        </div>
        <?php else:?>
            <span class="help-block" style="font-size:8pt"><i class="fa fa-warning"></i> Shared Content Not Available</span>
            <?php endif;?>
        <?php endif;?>
    </div>

    <div class="clearfix"></div>
    <div class="post-footer" style="margin-right:8px">
        <?php
            echo $this->Form->textarea($post['id'],
                    [ 'label' => false, 'placeholder' => 'Comment',
                        'onkeyup' => 'textAreaAdjust(this)',
                        'class' => 'form-control comment'
                    ]);

            $unlike = '';
//            foreach ($post['Upvote'] as $like)  {
//                if ($like['user_id'] == $session['User']['id'] && $like['comment_id'] === NULL) {
//                    $unlike = 'unlike';
//                }
//            }
        ?>

        <span class="footer-btn" style="width: 38%; text-align: center">
            <a class="btn btn-xs <?php //echo $unlike;?>" onclick="likePost(<?php echo $post['id']; ?>,this)"><?php //echo count($post['Upvote']);?> <i class="fa fa-heart-o"></i> Likes
            </a> &nbsp;
            <a class="btn btn-xs" onclick="share(<?php echo $post['id']; ?>)"> <?php //echo count($post['Share']);?>
                <i class="fa fa-retweet"></i> Share
            </a>
        </span>
    </div>
</div>

<script>
$("textarea").focus(function(e) {
    var elem = $(this)
    elem.css('width', '100%');
    elem.parent().find('.footer-btn').hide();
});

$("textarea").focusout(function(e) {
    var elem = $(this)
    elem.val(null);
    elem.css({
        'width': '68%',
        'height': '32px'
    });
    elem.parent().find('.footer-btn').show();
});

function textAreaAdjust(o) {
    var scroll_height = $(o).get(0).scrollHeight;
    $(o).css('height', scroll_height + 'px');
}
</script>
