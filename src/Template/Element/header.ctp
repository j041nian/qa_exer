<?php
    $session = $this->Session->read('Auth');
?>
<nav class="navbar navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <?php echo $this->Html->link('MicroBlog',
                    ['controller' => 'pages', 'action'=>'home'],
                    ['escape' => false, 'class' => 'navbar-brand']);
            ?>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <?php
                        /*
                         * Search Bar Form
                         */
                        echo $this->Form->create('Pages', ['url' => ['controller'=>'pages','action'=>'search']]);
                        echo $this->Form->input('string', [
                                'label' => false,
                                'class' => 'form-control searchbar',
                                'placeholder' => ' Search',
                                'style' => 'font-family:Arial, FontAwesome'
                            ]);
                        echo $this->Form->end();
                    ?>
                </li>
                <li>
                    <div class="dropdown">
                        <a class="dropdown-toggle fa fa-bars" data-toggle="dropdown"></a>
                        <ul class="dropdown-menu">
                            <li class='profile-dropdown'>
                                <div class="profile-rec">
                                    <div class="profile-bubble">

                                        <?php
                                            /*
                                             * Dropdown Menu Profile Picture
                                             */
                                            if (!empty($session['User']['Photo']['photo_name']))
                                                echo $this->Html->image($session['User']['username']."/".$session['User']['Photo']['photo_name'],
                                                    ['class' => 'profile-circle']);
                                            else
                                                echo $this->Html->image('user.png', ['class' => 'profile-circle']);
                                        ?>

                                    </div>

                                </div>
                            </li>
                            <li>
                                <?php echo $this->Html->link('Profile',['controller'=>'pages','action'=>'home'])?>
                            </li>
                            <li>
                                <?php echo $this->Html->link('Settings',['controller'=>'settings','action'=>'index'])?>
                            </li>
                            <li>
                                <?php
                                    echo $this->Html->link('Logout',
                                        ['controller'=>'users','action'=>'logout'],
                                        ['id' => 'logout']);
                                ?>
                            </li>
                        </ul>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>

<script>
$(".searchbar").on('focus', function() {
    $(this).animate({
        width: '300px'
    }, 300);
})

$(".searchbar").on('focusout', function() {
    $(this).animate({
        width: '200px'
    }, 300);
})

$(".searchbar").keypress(function(e) {
    if (e.keyCode == 13) {
        $("#PagesSearchForm").submit();
    }
});

$("#logout").click(function(e) {
    var id = this.id;
    $("#ajaxModal").modal({
        backdrop: 'static',
        keyboard: false,
        show: true
    });
});

</script>
