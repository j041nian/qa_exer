<?php
use App\Form\UsersForm;

if (!isset($params['escape']) || $params['escape'] !== false) {
    $message = h($message);
}
?>
<div class="flash banner-error animated fadeInDown" onclick="this.classList.add('hidden');" data-toggle="tooltip" title="Close Message">
    <i class="fa fa-warning"></i> <?= $message ?>
</div>
