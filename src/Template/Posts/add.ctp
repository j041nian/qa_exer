        <!-- Modal content-->
<div class="modal-content animated zoomIn">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Write New Post:</h4>
    </div>
    <div class="modal-body">
        <?php echo $this->Form->create('Post', ['type' => 'file', 'id' => 'postForm']);?>
        <?php echo $this->Form->textarea('content', [
            'placeholder' => "What's up?",
            'class' => 'post-area',
            'id' => 'post_content'
        ]);?>
        <span class="help-block pull-right" class="dropdown-toggle" data-toggle="dropdown"><i class="count-char">0</i> / 140 char</span>
        <span class="pull-left">
                <div class="upload-btn-wrapper">
                    <button class="btn btn-sm btn-custom" style="font-size:8pt;"><i class="fa fa-image"></i> Photo</button>
                    <?php
                      echo $this->Form->input('img_upload', [
                                'type'=>'file',
                                'label' => false,
                                'onchange' => "preview_image();"
                        ]);
                    ?>
                    <?php echo $this->Form->end(); ?>
                </div>
        </span>
        <div class="profile-rec">
            <div class="profile-bubble">
                <?php if (!empty($photo)): ?>
                <?php echo $this->Html->image($username."/".$photo['Photo']['photo_name'], ['class' => 'profile-circle']);?>
                <?php else: ?>
                <?php echo $this->Html->image("user.png", ['class' => 'profile-circle']);?>
                <?php endif; ?>
            </div>
            <span class="profile-name"></span>
        </div>

    </div>
    <div class="clearfix"></div>
    <div class="ajaxStat" style="padding:0 10px 0px 10px"></div>
    <div class="modal-footer">
        <div class="col-md-2 col-sm-3 pull-right">
            <div class="row">
                <?php
                    echo $this->Form->button('Post', [
                        'class' => 'btn btn-red btn-custom btn-submit',
                        'form' => 'postForm'
                    ]);
                ?>
            </div>
        </div>
        <div class="col-md-2 col-sm-3">
            <div class="row">
                <button type="button" class="btn btn-custom" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>

<script>
$(document).ready(function() {
    $("#post_content").on('input', function(e) {
        var post = $(this).val();
        $(".count-char").html(post.length);
    });
});

function preview_image() {
    $('.ajaxStat').html("<img width='25%' src='" + URL.createObjectURL(event.target.files[0]) + "' style='padding:10px'>");
}
</script>
