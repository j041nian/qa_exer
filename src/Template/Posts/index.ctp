<div style="border:1px solid black; margin-top: -22px">
    <div class="col-lg-2 col-md-2 col-sm-1 col-xs-1"></div>
    <div class="col-lg-8 col-md-8 col-sm-10 col-xs-10 whiteBackground">
        <div class="row">
            <div class="col-lg-12">
                <!-- FOR CREATING YOUR POST -->
                <?= $this->Form->create('Post',
                    ['enctype' => 'multipart/form-data']
                ) ?>
                <div class="row">
                    <div class="col-lg-1 col-md-1 col-sm-2 col-xs-3">
                        <br>
                        <?php
                            //FOR DISPLAYING YOUR PROFILE PICTURE
                            echo $this->Html->link($this->Html->image('/webroot/img/uploads/images/'.$myProfilePicture,
                            ['width' => '45', 'height' => '45', 'class' => 'img-circle pull-right']),
                            '#', ['escape' => false]);
                       ?>
                    </div>
                    <div class="col-lg-11 col-md-11 col-sm-10 col-xs-9">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <?php
                                echo $this->Form->control('',
                                    [
                                        'type' => 'textarea',
                                        'name' => 'content',
                                        'class' => 'form-control',
                                        'placeholder' => "What's on your mind?",
                                        'maxlength' => '140',
                                        'height' => '150px',
                                        'required' => 'required',
                                        'resize' => 'none'
                                    ]
                                );
                            ?>
                        </div>
                        <!-- FOR SHOWING PICTURE BEFORE POSTING -->
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <?php
                                echo $this->Html->image('/webroot/img/uploads/images/',
                                [
                                    'width' => '200', 
                                    'height' => '200', 
                                    'id' => 'postImage', 
                                    'class' => 'img-responsive'
                                ]); 
                            ?>
                        </div>
                    </div>
                </div>
                <br/>
                <!-- FOR POST PICTURE AND SAVING -->
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <?php
                            echo $this->Form->submit(__('Post'),[
                                'class'=>'btn btn-success pull-right',
                                'style' => 'border-radius: 200px; width: 100px',
                            ]);
                            echo $this->Form->control('',
                                [
                                    'onchange' => 'readURL(this)',
                                    'type' => 'file',
                                    'name' => 'picture',
                                    'class' => 'file form-control pull-right',
                                    'style' => 'width: 150px'
                                ]
                            );
                            echo $this->Form->end();
                        ?>
                    </div>
                </div>
                <!-- END OF CREATING YOUR POST -->
                <hr style="height: 2px; background-color: black">
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <!-- FOR PRODUCING A ROW PER POST -->
                <?php 
                    $postCount = 0;
                    foreach ($posts as $post) { 
                ?>
                <div class="row">
                    <!-- FOR POST CONTENT-->
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> 
                        <br>
                        <!-- FOR PROFILE PICTURE OF THE POSTER -->
                        <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">
                            <?php
                                echo $this->Html->link($this->Html->image("/webroot/img/uploads/images/". $post['poster']['profile_picture'],
                                ['width' => '45', 'height' => '45', 'class' => 'img-circle']) ,
                                [
                                    'action' => 'view', 
                                    $post['poster']['id'] , 
                                    'controller' => 'users'
                                ],
                                ['escape' => false]);
                           ?>
                        </div>
                        <?php
                            if ($currentUserId === $post['poster_user_id']) { 
                        ?>
                        <div class="btn-group" role="group">
                            <button type="button" class="btn btn-link dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" role="menu">
                                <li>
                                    <?= $this->Html->link($this->Html->tag('i', '', ['class' => 'fa fa-edit']).' Edit', ['action' => 'edit', $post['id']], ['escape' => false]) ?>
                                </li>
                                <li>
                                    <button type="button" id="deletePost<?= $post['id'] ?>" onclick="deletePost(this.id)" class="btn btn-link">
                                        <span class='fa fa-trash'></span>
                                        Delete
                                    </button>
                                </li>
                            </ul>
                        </div>
                        <!-- FOR EDIT AND DELETE BUTTON IF YOU ARE THE POST AUTHOR-->
                        <?php } ?>
                        <div class="col-lg-10 col-md-10 col-sm-8 col-xs-8">
                            <?php
                                $fullname = ucwords($post['poster']['firstname'].' '.$post['poster']['lastname']);
                                echo $this->Html->link($fullname, ['action' => 'view', $post['poster']['id'] , 'controller' => 'users']); 
                                echo '<br>';
                                //Checks if the post content was edited
                                // $timeCount = 0;
                                // foreach ($post->created as $created) {
                                //     $created = $created;
                                //     break;
                                // }
                                // $timeCount = 0;
                                // foreach ($post->modified as $modified) {
                                //     $modified = $modified;
                                //     break;
                                // }
                                $created = $post['created'];
                                $modified = $post['modified'];

                                if ($created === $modified) {
                                    echo 'Created on '.date('F j, Y g:i A', strtotime($created));
                                } else {
                                    echo 'Edited on '.date('F j, Y g:i A', strtotime($modified));
                                }
                            ?>
                        </div>
                        <br><br>&nbsp;
                        <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                            <div class='col-lg-10 col-md-10'>
                                <?php
                                    // print_r($post['Post']);
                                    //FOR POST CONTENT
                                    echo h($post['content']);
                                ?>
                                <div style="margin-left: 40%;">
                                    <?php 
                                        echo $this->Html->image('/webroot/img/uploads/images/'.$post['picture'],
                                        ['width' => '200', 'height' => '200', 'class' => 'img-responsive']);
                                        echo $this->Form->hidden('postId'.$post['id'], 
                                            ['value' => $post['id']]
                                        );
                                    ?>
                                </div>
                            </div>
                            <?php 
                                //FOR RETWEET CONTENT
                                if ($post['retweet_user_id'] !== null) {
                            ?>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #d7e9ea"><br>
                                <?php
                                    //CHECKS IF THE CONTENT IS DELETED
                                    if ($post['retweet']['deleted'] !== null) {
                                        echo 'Content not available. <br> &nbsp';
                                    } else {
                                ?>
                                <div class="col-lg-1 col-md-1 col-sm-2 col-xs-3">
                                    <?php 
                                        echo $this->Html->link($this->Html->image("/webroot/img/uploads/images/". $post['retweet_author']['profile_picture'],
                                            ['width' => '45', 'height' => '45', 'class' => 'img-circle']),
                                            [
                                                'action' => 'view', $post['retweet_author']['id'], 
                                                'controller' => 'users'
                                            ],
                                            ['escape' => false]) 
                                    ?>
                                </div>
                                <div class="col-lg-10 col-md-10 col-sm-8 col-xs-9">
                                    <?php 
                                        $fullname = ucwords($post['retweet_author']['firstname'].' '.$post['retweet_author']['lastname']);
                                        echo $this->Html->link(h($fullname), ['action' => 'view', $post['retweet_author']['id'], 'controller' => 'users']); 
                                        echo '<br>';

                                        // $timeCount = 0;
                                        // foreach ($post->retweet->created as $created) {
                                        //     $created = $created;
                                        //     break;
                                        // }
                                        // $timeCount = 0;
                                        // foreach ($post->retweet->modified as $modified) {
                                        //     $modified = $modified;
                                        //     break;
                                        // }

                                        $created = $post['retweet']['created'];
                                        $modified = $post['retweet']['modified'];

                                        if ($created === $modified) {
                                            echo 'Created on '.date('F j, Y g:i A', strtotime($created));
                                        } else {
                                            echo 'Edited on '.date('F j, Y g:i A', strtotime($modified));
                                        }
                                    ?>
                                </div><br>
                                <div class='col-lg-10 col-md-10 col-'>
                                    <br>
                                    <?php
                                        //FOR RETWEET POST CONTENT
                                        echo h($post['retweet']['content']);
                                    ?>
                                    <div style="margin-left: 40%;">
                                        <?php echo $this->Html->image('/webroot/img/uploads/images/'.$post['retweet']['picture'],
                                            [
                                                'width' => '200', 
                                                'height' => '200', 
                                                'class' => 'img-responsive'
                                            ]) ?>
                                        <br>&nbsp;
                                    </div>
                                </div>
                                <?php }?>
                            </div>
                            <?php
                                }
                                //END OF RETWEET CONTENT
                            ?>
                            <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> 
                        <br>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <?php
                                //FOR LIKE BUTTON
                                $likerCount = count($post['likes']);
                                $likeClass = 'stroke-transparent';
                                $goTo = 'likePost(this.id)';
                                $title = 'Click to like this post';
                                $userId = '0';
                                $actionId = $post['id'];
                                foreach ($post['likes'] as $like) {
                                    if ($like['liker_user_id'] === $currentUserId) {
                                        $likeClass = '';
                                        $goTo = 'unlikePost(this.id)';
                                        $title = 'Click to unlike this post';
                                        // $actionId = $like['id'];
                                        $userId = $like['liker_user_id'];
                                    }
                                }
                            ?>
                                <label id="postLikeLabel<?= $post['id'] ?>"><?= $likerCount > 0 ? $likerCount : '' ?></label>
                            <?php
                                // echo  $likerCount > 0 ? $likerCount : '';
                                echo $this->Form->button('<i class="fa fa-thumbs-up '.$likeClass.'" id="likeIcon'.$post['id'].'"></i> Like', [
                                    'type' => 'button',
                                    'title' => $title,
                                    'id' => 'like'.$actionId,
                                    'class' => 'btn btn-link',
                                    'onclick' => $goTo
                                ]);
                                //FOR RETWEET BUTTON
                                $retweetCount = count($post['retweeters']);
                                echo $retweetCount > 0 ? $retweetCount : '';
                                $retweetClass = 'stroke-transparent';
                                $title = 'Click to retweet this post';
                                $goTo = 'retweetPost(this.id)';
                                $text = 'Retweet';
                                if ($post['retweet_user_id'] !== null && $post['poster_user_id'] === $currentUserId) {
                                    $retweetUserId = $post['retweet_user_id'];
                                    $retweetPostId = $post['retweet_post_id'];
                                    if ($post['poster_user_id'] === $currentUserId) {
                                        $retweetClass = '';
                                        $title = 'Click to untweet this post';
                                        $text = 'Untweet';
                                        $retweetUserId =  $post['retweet_user_id'];
                                        $retweetPostId = $post['id'];
                                        $goTo = 'unTweetPost(this.id)';

                                        //FOR UNTWEET BUTTON
                                        echo $this->Form->button('<i class="fa fa-retweet '.$retweetClass.'"></i> '.$text, [
                                            'type' => 'button',
                                            'title' => $title,
                                            'id' => 'retweet'.$post['id'],
                                            'class' => 'btn btn-link',
                                            'onclick' => $goTo
                                        ]);
                                    }
                                } else {
                                    if ($post['retweet_user_id'] === null) {
                                        $retweetUserId =  $post['poster_user_id'];
                                        $retweetPostId = $post['id'];
                                        $retweetProfilePicture = $post['poster']['profile_picture'];
                                        $retweetName = $post['poster']['firstname'].' '.$post['poster']['lastname'];
                                        $retweetContent = $post['content'];
                                        $retweetPicture = $post['picture'];
                                    } else {
                                        $retweetUserId =  $post['retweet_author']['id'];
                                        $retweetPostId = $post['retweet']['id'];
                                        $retweetProfilePicture = $post['retweet_author']['profile_picture'];
                                        $retweetName = $post['retweet_author']['firstname'].' '.$post['retweet_author']['lastname'];
                                        $retweetContent = $post['retweet']['content'];
                                        $retweetPicture = $post['retweet']['picture'];
                                    }

                                    //FOR TWEET BUTTON
                                    echo $this->Form->button('<span class="fa fa-retweet '.$retweetClass.'"></span> '.$text, [
                                        'type' => 'button',
                                        'title' => $title,
                                        'data-toggle' => 'modal',
                                        'data-target' => '#retweetModal'.$post['id'],
                                        'id' => 'retweet'.$post['id'],
                                        'class' => 'btn btn-link',
                                    ]);
                                ?>
                                <!-- RETWEET MODAL -->
                                <div class="modal fade" id="retweetModal<?= $post['id'] ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                                <h4 class="modal-title text-center" id="exampleModalLabel">Retweet Post </h4>
                                            </div>
                                            <div class="modal-body">
                                                <!-- RETWEET CONTENT -->
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <!-- FOR CREATING A RETWEET COMMENT -->
                                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-3">
                                                        <br>
                                                        <?php 
                                                            echo $this->Html->link($this->Html->image('/webroot/img/uploads/images/'.$myProfilePicture,
                                                            [
                                                                'width' => '45', 
                                                                'height' => '45', 
                                                                'style' => '',
                                                                'class' => 'img-circle'
                                                            ]) ,
                                                            '#', ['escape' => false]);
                                                       ?>
                                                    </div>
                                                    <div class="col-lg-11 col-md-11 col-sm-11 col-xs-9">
                                                        <?php
                                                            echo $this->Form->control('retweetContent'.$post['id'],
                                                                [
                                                                    'type' => 'textarea',
                                                                    'class' => 'form-control',
                                                                    'placeholder' => 'Write your thoughts here',
                                                                    'id' => 'retweetContent'.$post['id'],
                                                                    'maxlength' => '140',
                                                                    'height' => '100px',
                                                                    'rows' => 3,
                                                                    'resize' => 'none',
                                                                    'label' => false
                                                                ]
                                                            );
                                                        ?>
                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #d7e9ea"><br>
                                                            <?php
                                                                //CHECKS IF THE CONTENT IS DELETED
                                                                if ($post['retweet'] !== null) {
                                                                    echo 'Content not available. <br> &nbsp';
                                                                } else {
                                                            ?>
                                                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-3">
                                                                <?php
                                                                    echo $this->Html->link($this->Html->image("/webroot/img/uploads/images/". $retweetProfilePicture, [
                                                                        'width' => '45', 
                                                                        'height' => '45', 
                                                                        'class' => 'img-circle'
                                                                    ]), [
                                                                        'action' => 'view', $retweetUserId, 
                                                                        'controller' => 'users'
                                                                    ], ['escape' => false]) 
                                                                ?>
                                                            </div>
                                                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-9">
                                                            <?php 
                                                                if ($post['retweet_author'] !== null) {
                                                                    echo $this->Html->link(ucwords($retweetName), ['action' => 'view', $post['retweet_author']['id'], 'controller' => 'users']); 
                                                                    echo '<br>';
                                                                    //Checks if the retweet content was edited
                                                                    $timeCount = 0;
                                                                    foreach ($post->retweet->created as $created) {
                                                                        $created = $created;
                                                                        break;
                                                                    }
                                                                    $timeCount = 0;
                                                                    foreach ($post->modified as $modified) {
                                                                        $modified = $modified;
                                                                        break;
                                                                    }

                                                                    if ($created === $modified) {
                                                                        echo 'Created on '.date('F j, Y g:i A', strtotime($created));
                                                                    } else {
                                                                        echo 'Edited on '.date('F j, Y g:i A', strtotime($modified));
                                                                    }
                                                                }
                                                            ?>
                                                        </div><br>
                                                        <div class='col-lg-10 col-md-10 col-'>
                                                            <br>
                                                            <?php
                                                                //FOR RETWEET POST CONTENT
                                                                echo h($retweetContent);
                                                            ?>
                                                            <div style="margin-left: 40%;">
                                                                <?php echo $this->Html->image('/webroot/img/uploads/images/'.$retweetPicture,
                                                                ['width' => '200', 'height' => '200', 'class' => 'img-responsive']) ?>
                                                                <br>&nbsp;
                                                            </div>
                                                        </div>
                                                            <?php } ?>
                                                       </div>
                                                        <!-- END OF CREATING YOUR RETWEET CONTENT -->
                                                    </div>
                                                </div>
                                                <!-- END OF RETWEET CONTENT -->&nbsp;
                                            </div>
                                            <div class="modal-footer">
                                                <?php 
                                                    echo $this->Form->button('<i class="fa fa-retweet '.$retweetClass.'"></i> '.$text, [
                                                            'type' => 'button',
                                                            'title' => $title,
                                                            'id' => 'retweet'.$post['id'],
                                                            'class' => 'btn btn-link',
                                                            'onclick' => $goTo
                                                        ]
                                                    );
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- END OF EDIT MODAL -->
                                <?php
                                }
                                echo $this->Form->hidden('retweetPostId'.$post['id'], [
                                    'value' => $retweetPostId,
                                    'id' => 'retweetPostId'.$retweetPostId
                                ]);
                                echo $this->Form->hidden('retweetPosterUserId'.$post['id'], [
                                    'value' => $retweetUserId,
                                    'id' => 'retweetPosterUserId'.$post['id']
                                ]);
                            ?>
                            <hr style="height: 1px; background-color: black">
                            <!-- FOR COMMENTS -->
                            Comments
                            <?php 
                                $commentCtr = 0;
                                foreach($post['comments'] as $comment) {
                            ?>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="col-lg-1 col-md-1 col-sm-2 col-xs-3">
                                    <?php 
                                        foreach($users as $user) {
                                            if ($user['id'] === $comment['user_id']) {
                                                $fullname = $user['firstname']. ' '.$user['lastname'];
                                                $fullname = ucwords($fullname);
                                                $picture = $user['profile_picture'];
                                            }
                                        }
                                        echo $this->Html->link($this->Html->image("/webroot/img/uploads/images/". $picture,
                                        ['width' => '45', 'height' => '45', 'class' => 'img-circle']) ,
                                        ['action' => 'view', $comment['user_id'] , 'controller' => 'users'], 
                                        ['escape' => false]) 
                                    ?>
                                </div>
                                <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <?php
                                            //$fullname = 'Frenz Delgado';
                                            echo $this->Html->link($fullname, [
                                                'action' => 'view', 
                                                $comment['user_id'], 
                                                'controller' => 'users'
                                            ]); 
                                            echo " | ";
                                            $timeCount = 0;
                                            $modified = $comment['modified'];
                                            // foreach ($comment['modified'] as $modified) {
                                            //     $modified = $modified;
                                            //     break;
                                            // }
                                            echo date('F j, Y g:i A', strtotime($modified));
                                        ?>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <?= h($comment['comment']) ?>
                                    </div>
                                </div>
                                <?php if ($comment['user_id'] === $currentUserId) { ?>
                                <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                                    <!-- FOR EDIT AND DELETE -->
                                    <button type="button" class="btn btn-link dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                        <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu" role="menu">
                                        <li>
                                            <button type="button" class="btn btn-link" data-toggle="modal" data-target="#editCommentModal<?= $comment['id'] ?>">
                                                <span class='fa fa-edit'></span>
                                                Edit
                                            </button>
                                        </li>
                                        <li>
                                            <button type="button" id="deleteComment<?= $comment['id'] ?>" onclick="deleteComment(this.id)" class="btn btn-link">
                                                <span class='fa fa-trash'></span>
                                                Delete
                                            </button>
                                        </li>
                                    </ul>
                                </div> 
                                <!-- EDIT MODAL -->
                                <div class="modal fade" id="editCommentModal<?= $comment['id'] ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                                <h4 class="modal-title text-center" id="exampleModalLabel">Edit your comment </h4>
                                            </div>
                                            <div class="modal-body">
                                                <!-- COMMENT CONTENT -->
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <!-- FOR CREATING A COMMENT -->
                                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-3">
                                                        <br>
                                                        <?php 
                                                            echo $this->Html->link($this->Html->image('/webroot/img/uploads/images/'.$myProfilePicture, [
                                                                    'width' => '45', 
                                                                    'height' => '45', 
                                                                    'style' => '',
                                                                    'class' => 'img-circle'
                                                                ]
                                                            ),
                                                            '#', ['escape' => false]);
                                                       ?>
                                                    </div>
                                                    <div class="col-lg-11 col-md-11 col-sm-11 col-xs-9">
                                                        <?php
                                                            echo $this->Form->control('commentIdContent'.$comment['id'], [
                                                                    'type' => 'textarea',
                                                                    'class' => 'form-control',
                                                                    'placeholder' => 'Write your comment here',
                                                                    'id' => 'commentIdContent'.$comment['id'],
                                                                    'maxlength' => '140',
                                                                    'height' => '100px',
                                                                    'rows' => 3,
                                                                    'resize' => 'none',
                                                                    'label' => false,
                                                                    'value' => $comment['comment']
                                                                ]
                                                            );
                                                            echo $this->Form->hidden('comment_id', 
                                                                ['value' => $comment['id']]
                                                            );
                                                        ?>
                                                        <!-- END OF CREATING YOUR COMMENT -->
                                                    </div>
                                                </div>
                                                <!-- END OF COMMENT CONTENT -->&nbsp;
                                            </div>
                                            <div class="modal-footer">
                                                <?php 
                                                    echo $this->Form->button(__('Comment'), [
                                                        'class'=>'btn btn-success pull-right',
                                                        'style' => 'border-radius: 200px; width: 100px',
                                                        'onclick' => 'editComment(this.id)',
                                                        'id' => 'editComment'.$comment['id']
                                                    ]);
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php } ?><br><br>&nbsp;
                                <!-- END OF EDIT MODAL -->
                            </div>
                            <?php
                                $commentCtr++;
                                if ($commentCtr > 2) {
                                     echo "<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>".$this->Html->link('View more comments', [
                                            'action' => 'view', $post['id'], 
                                            'controller' => 'posts'
                                        ]
                                    )."</div>"; 
                                    break;
                                }
                            } 
                            ?>
                            <!-- END OF COMMENTS -->
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <!-- FOR CREATING A COMMENT -->
                                <div class="col-lg-1 col-md-1 col-sm-1 col-xs-3">
                                    <br>
                                    <?php 
                                        echo $this->Html->link($this->Html->image('/webroot/img/uploads/images'.$myProfilePicture, [
                                                'width' => '45', 
                                                'height' => '45', 
                                                'style' => '',
                                                'class' => 'img-circle'
                                            ]
                                        ), '#', ['escape' => false]);
                                   ?>
                                </div>
                                <div class="col-lg-11 col-md-11 col-sm-11 col-xs-9">
                                    <?php
                                        echo $this->Form->control('commentContent', [
                                                'type' => 'textarea',
                                                'id' => 'commentContent'.$post['id'],
                                                'class' => 'form-control',
                                                'placeholder' => 'Write your comment here',
                                                'maxlength' => '140',
                                                'height' => '100px',
                                                'rows' => 3,
                                                'resize' => 'none',
                                                'label' => false
                                            ]
                                        );
                                        echo $this->Form->button(__('Comment'), [
                                            'class'=>'btn btn-success pull-right',
                                            'style' => 'border-radius: 200px; width: 100px',
                                            'id' => $post['id'],
                                            'onclick' => 'addComment(this.id)'
                                        ]);
                                    ?>
                                    <!-- END OF CREATING YOUR COMMENT -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END OF COMMENT-->
                </div>
                <!-- END OF POST ROW-->
                <hr style="height: 2px; background-color: black">
                <?php } ?>
            </div>
        </div>
        <?php
            // echo $this->Paginator->numbers([
            //     'before' => '<nav aria-label="Page navigation example"><ul class="pagination">',
            //     'currentClass' => 'active',
            //     'tag' => '<li class="page-item">',
            //     'after' => '</ul></nav>'
            // ]);
        ?>
        <!-- END OF WHOLE PAGE -->
    </div>
</div>