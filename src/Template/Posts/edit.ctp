<div style="border:1px solid black; margin-top: -22px">
    <div class="col-lg-2 col-md-2 col-sm-1 col-xs-1"></div>
    <div class="col-lg-8 col-md-8 col-sm-10 col-xs-10" style="background-color:white">
        <div class="row">
            <div class="col-lg-12">
                <button class="btn btn-link" onclick="window.history.back();">Back</button>
                <!-- FOR CREATING YOUR POST -->
                <h3 class="text-center">Edit Your Post</h3>
                <?php echo $this->Form->create('Post',
                    ['enctype' => 'multipart/form-data']
                ) ?>
                <div class="row">
                    <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">
                        <br>
                        <?php
                            //FOR DISPLAYING YOUR PROFILE PICTURE
                            echo $this->Html->link($this->Html->image('/webroot/img/uploads/images/'.$myProfilePicture,
                            ['width' => '45', 'height' => '45', 'class' => 'img-circle pull-right']),
                            '#', ['escape' => false]);
                       ?>
                    </div>
                    <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <?php
                                echo $this->Form->control('content',
                                    [
                                        'type' => 'textarea',
                                        'name' => 'content',
                                        'class' => 'form-control',
                                        'value' => $post['content'],
                                        'placeholder' => "What's on your mind?",
                                        'maxlength' => '140',
                                        'height' => '150px',
                                        'required' => 'required',
                                        'resize' => 'none',
                                        'label' => ''
                                    ]
                                );
                            ?>
                        </div>
                        <?php 
                            //FOR RETWEET CONTENT
                            if ($post['retweet_user_id'] !== null) {
                        ?>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #d7e9ea"><br>
                            <?php
                                //CHECKS IF THE CONTENT IS DELETED
                                if ($post['retweet']['deleted'] !== null) {
                                    echo 'Content not available. <br> &nbsp';
                                } else {
                            ?>
                            <div class="col-lg-1 col-md-1 col-sm-2 col-xs-3">
                                <?=$this->Html->link($this->Html->image("/webroot/img/uploads/images/". $post['retweet_author']['profile_picture'],
                                    ['width' => '45', 'height' => '45', 'class' => 'img-circle']),
                                    ['action' => 'view', $post['retweet_author']['id'], 'controller' => 'users'],
                                    ['escape' => false]) ?>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-9">
                                <?php 
                                    $fullname = $post['retweet_author']['firstname'].' '.$post['retweet_author']['lastname'];
                                    echo $this->Html->link($fullname, ['action' => 'view', $post['retweet_author']['id'], 'controller' => 'users']); 
                                    echo '<br>';
                                    //Checks if the retweet content was edited
                                    $timeCount = 0;
                                    foreach ($post['created'] as $created) {
                                        $created = $created;
                                        break;
                                    }
                                    $timeCount = 0;
                                    foreach ($post['modified'] as $modified) {
                                        $modified = $modified;
                                        break;
                                    }

                                    if ($created === $modified) {
                                        echo 'Created on '.date('F j, Y g:i A', strtotime($created));
                                    } else {
                                        echo 'Edited on '.date('F j, Y g:i A', strtotime($modified));
                                    }
                                ?>
                            </div><br>
                            <div class='col-lg-10 col-md-10 col-'>
                                <br>
                                <?php
                                    //FOR RETWEET POST CONTENT
                                    echo $post['retweet']['content'];
                                ?>
                                <div style="margin-left: 40%;">
                                    <?php 
                                        if ($post['retweet']['picture'] !== null ) {
                                            echo $this->Html->image('/webroot/img/uploads/images/'.$post['retweet']['picture'], [
                                                'width' => '200', 
                                                'height' => '200', 
                                                'class' => 'img-responsive'
                                            ]);
                                        }
                                    ?>
                                    <br>&nbsp;
                                </div>
                            </div>
                        </div>
                        <?php
                                }
                            }
                            //END OF RETWEET CONTENT
                        ?>
                        <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">
                        </div>
                        <!-- FOR SHOWING PICTURE BEFORE POSTING -->
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <?php
                                echo $this->Html->image('/webroot/img/uploads/images/'.$post['picture'], [
                                    'width' => '200', 
                                    'height' => '200', 
                                    'name' => 'picture', 
                                    'id' => 'postImage', 
                                    'class' => 'img-responsive'
                                ]); 
                            ?>
                        </div>
                    </div>
                </div>
                <br/>
                <!-- FOR POST PICTURE AND SAVING -->
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <?php
                            echo $this->Form->submit(__('Post'), [
                                'class'=>'btn btn-success pull-right',
                                'style' => 'border-radius: 200px; width: 100px',
                            ]);
                            if ($post['retweet'] !== '') {
                                echo $this->Form->control('picture', [
                                    'type' => 'file',
                                    'onchange' => 'readURL(this)',
                                    'name' => 'picture',
                                    'class' => 'file form-control pull-right',
                                    'style' => 'width: 150px',
                                    'label' => ''
                                ]);
                            }
                        ?>
                    </div>
                </div><br/>
                <!-- END OF CREATING YOUR POST -->
            </div>
        </div>
    </div>
</div>