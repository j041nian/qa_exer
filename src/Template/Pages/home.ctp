<?php
    $session = $this->Session->read('Auth');
?>
    <style>
        .banner {
            <?php
                if (!empty($cover['Photo'])) {
                    echo "background: url('img/".$session['User']['firstname']."/".$cover['Photo']['firstname']."');";
                }
            ?>
            background-repeat: no-repeat;
            background-size: cover;
        }
    </style>

    <div class="jumbotron banner">
        <div class="col-md-1 col-sm-1">
            <a href="#" onclick="cover()" class="btn btn-sm btn-default">
            <span class="fa fa-pencil"></span> Cover
        </a>
        </div>
        <!-- /.col-md-1 -->
    </div>
    <!-- /.banner -->

    <div class="stats animated slideInDown">
        <div class="col-lg-offset-3 col-lg-9 col-md-9">
            <div class="profile-picture">

                <?php if (!empty($session['User']['Photo']['photo_name'])) :?>
                <img src="img/<?php echo $session['User']['username']."/". $session['User']['Photo']['photo_name'] ?>">
                <?php else :?>
                <img src="img/user.png">
                <?php endif;?>

                <div class="button btn">
                    <a href="#" onclick="upload()">
                    <span class="fa fa-upload"></span> Change picture
                </a>
                </div>

            </div>
            <!-- /.profile-picture -->

            <div class="account-name">
                <h1>
                    <?php echo $session['User']['firstname']." ".$session['User']['lastname']; ?>
                </h1>
                <h4>@
                    <?php echo $session['User']['firstname']; ?>
                </h4>
            </div>
            <!-- /.account-name -->

            <ul class="stats-list">
                <li class="col-md-2 col-sm-1">
                    <a onclick="function(e){e.preventDefault();}" class="btn btn-custom btn-sm">
                        <?php echo $likes;?> <i class="fa fa-heart-o"></i> Likes
                    </a>
                </li>
                <li class="col-md-2 col-sm-1">
                    <a onclick="function(e){e.preventDefault();}" class="btn btn-custom btn-sm">
                        <?php echo $followers;?> <i class="fa fa-users"></i> Followers
                    </a>
                </li>
                <li class="col-md-2 col-sm-1">
                    <a onclick="function(e){e.preventDefault();}" class="btn btn-custom btn-sm">
                        <?php echo $follows;?> <i class="fa fa-user-plus"></i> Following
                    </a>
                </li>
                <li class="col-md-2 col-sm-1">
                    <a onclick="function(e){e.preventDefault();}" class="btn btn-custom btn-sm">
                        <?php echo $shared;?> <i class="fa fa-share-square"></i> Retweets
                    </a>
                </li>
            </ul>
            <!-- /.stats-list -->
        </div>
        <!-- /.col-md-9 -->
    </div>
    <!-- /.stats -->

    <div class="container">
        <div class="col-md-3" style="height:100%">
            <div class="row side-info">
                <div class="col-md-12 col-sm-12 info animated bounceInLeft">
                    <div class="row">
                        <b><?php echo $session['User']['firstname']." ".$session['User']['lastname'];?></b> Joined Microblog <br>
                        <?php echo date("M d, Y",strtotime($session['User']['created']));?>
                    </div>
                </div>
                <!-- /.col-md-12 -->

                <div class="col-md-12 col-sm-12 info2 animated bounceInLeft">
                    <div class="row">
                        <ul class="list-group">
                            <li class="list-group-item">
                                <span class="pull-right">
                            <?php
                                echo $this->Html->link('<span class="fa fa-pencil"></span> Edit', array( 'controller' => 'settings', 'action' => 'index' ), array( 'class' => 'btn btn-xs btn-default', 'escape' => false ) ); ?>
                                </span>
                                <h4>Additional Info:</h4>
                            </li>
                            <li class="list-group-item">
                                <span class="fa fa-address-card-o"></span><span> Address</span>
                                <p>
                                    <?php echo isset($session['User']['Setting']['address'])?$session['User']['Setting']['address']:" ";?>
                                </p>
                            </li>
                            <li class="list-group-item">
                                <span class="fa fa-phone"></span><span> Phone</span>
                                <p>
                                    <?php echo isset($session['User']['Setting']['contact'])?$session['User']['Setting']['contact']:" ";?>
                                </p>
                            </li>
                            <li class="list-group-item">
                                <span class="fa fa-envelope"></span><span> Email</span>
                                <p>
                                    <?php echo $session['User']['email_address'];?>
                                </p>
                            </li>
                        </ul>
                        <!-- /.list-group -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.col-md-12 -->

                <div class="col-md-12 col-sm-12 info2 animated bounceInLeft">
                    <div class="row">
                        <ul class="list-group">
                            <li class="list-group-item">
                                <span class="pull-right">
                                    <?php echo $this->Html->link('see more',array('controller'=>'photos','action'=>'gallery'),array('class' => 'btn btn-default btn-xs'));?>
                            </span>
                                <h4>Photos:</h4>
                            </li>
                            <li class="list-group-item">
                                <?php foreach ($album as $picture) : ?>
                                <?php
                                echo $this->Html->image($session['User']['username']."/".$picture['Photo']['photo_name'],
                                    array('style' => 'width:45%')
                                );
                            ?>
                                    <?php endforeach;?>
                            </li>
                        </ul>
                        <!-- /.list-group -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.col-md-12 -->

                <div class="col-md-12 col-sm-12 info2 animated bounceInLeft">
                    <div class="row">
                        <ul class="list-group">
                            <li class="list-group-item">
                                <h4>Trends:</h4>
                            </li>
                            <li class="list-group-item">
                                <?php foreach (array_slice($trends, 0, 5) as $trend): ?>
                                <p>
                                    <?php
                                    echo $this->Html->link($trend['value'],array('controller' =>'tags','action' =>'trends',$trend['value']),
                                        array(
                                            'class' => 'btn-link',
                                            'style' => 'font-weight: bold'
                                        )
                                    );
                                ?>

                                        <br>
                                        <i style="color:#808080"><?php echo $trend['count'];?>Tweets</i>
                                </p>
                                <?php endforeach;?>
                            </li>
                        </ul>
                        <!-- /.list-group -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.col-md-12 -->

            </div>
        </div>

        <div class="col-md-9">
            <div class="row">
                <div class="timeline">
                    <?= $this->element('timeline', ['posts' => $posts]);?>
                </div>
                <!-- /.timeline -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.col-md-9 -->

    </div>
    <!----------------- /.container ---------------------->

    <!-- Modal -->
    <div id="ajaxModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="loader"></div>
        </div>
    </div>

    <a id="<?=$session['User']['id']?>" class="newButton btn btn-red"><span class="fa fa-plus"></span></a>

<script>
$(".newButton").click(function(e) {
    var id = this.id;
    $("#ajaxModal").modal({
        backdrop: 'static',
        keyboard: false,
        show: true
    });

    $.ajax({
        async: true,
        type: "POST",
        url: "posts/add/" + id,
        success: function(result) {
            $("#ajaxModal .modal-dialog").html(result);
        }
    });
});

$('#ajaxModal').on('hidden.bs.modal', function() {
    $("#ajaxModal > .modal-dialog").html('<div class="loader"></div>');
});

function upload() {
    $('#ajaxModal').modal({
        backdrop: 'static',
        keyboard: false,
        show: true
    });
    $.ajax({
        url: 'photos/upload',
        success: function(result) {
            $('#ajaxModal > .modal-dialog').html(result);
        }
    });
}

function cover() {
    $('#ajaxModal').modal({
        backdrop: 'static',
        keyboard: false,
        show: true
    });
    $.ajax({
        url: 'photos/cover',
        success: function(result) {
            $('#ajaxModal > .modal-dialog').html(result);
        }
    });
}

</script>
