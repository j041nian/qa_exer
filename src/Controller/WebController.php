<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;

/**
 * Web Controller
 *
 *
 * @method \App\Model\Entity\Web[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class WebController extends AppController
{
    public $components = [
        'Api',
        'Flash',
        'Auth' => [
            'loginRedirect' => [
                'controller' => 'pages',
                'action' => 'home'
            ],
            'logoutRedirect' => [
                'controller' => 'users',
                'action' => 'login'
            ],
            'authenticate' => [
                'Form' => [
                    'userModel' => 'Users',
                    'passwordHasher' => 'default',
                    'fields' => [
                        'username' => 'email_address',
                        'password' => 'password'
                    ]
                ]
            ]
        ]
    ];

    /**
     *
     *
     */
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow(['register', 'logout', 'login', 'confirm']);
    }

    public function resultParser($response, $show = true)
    {
        $result = $response->getJson();
        switch($result['code']) {
            case 200:
                if ($show) {
                    $message = __($result['message']);
                    $this->Flash->success($message);
                }
                break;
            case 409:
                $message = __($result['message']);
                $this->Flash->error($message);
                break;
            default:
                $this->Flash->error(__($result['message']));
                return $this->redirect($this->Auth->logout());
        }

        return $result['data'] ?? [];
    }
}
