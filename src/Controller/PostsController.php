<?php
namespace App\Controller;

use App\Controller\WebController;
use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\Http\Client;
use Cake\ORM\TableRegistry;
use Cake\Utility\Security;

class PostsController extends WebController
{
    /**
     * for initializing the pagination
     *
     * @return CakeResponse|null
     */
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Paginator');
        $this->loadComponent('RequestHandler');
    }

    /**
     * for allowing specific pages without needing to login
     *
     * @param int $event checks if the user is allowed to visit specific spaces.
     * Allowed pages are declared in $this->Auth->allow()
     *
     * @return CakeResponse|null
     */
//    public function beforeFilter(Event $event)
//    {
//        parent::beforeFilter($event);
//        $this->Auth->allow(['index']);
//
//        return null;
//    }
//
//    /**
//     * Checks if the user is authorize to perform an action
//     *
//     * @param int $user checks if the current user id is authorized to perform an action
//     *
//     * @return bool
//     */
//    public function isAuthorized($user)
//    {
//        $allowedActions = ['index', 'add', 'view', 'edit', 'delete', 'retweet', 'viewRetweet'];
//        if (in_array($this->request->getParam('action'), $allowedActions)) {
//            return true;
//        }
//
//        return parent::isAuthorized($user);
//    }

    /**
     * for displaying all posts that the user follows in home page
     *
     * @return CakeResponse|null
     */
    public function index()
    {
        $data = [];
        if (!$this->Auth->user()) {
            $type = 'fail';
            $code = 401;
            $jsonMessage = 'Unauthorize Access';
            $data = null;

            if (!$this->request->is('json')) {
                return $this->redirect(['controller' => 'users', 'action' => 'login']);
            }
        } else {
            $currentUserId = $this->Auth->user('id');
            $token = $this->Auth->user('token');
        }
        if (isset($currentUserId)) {
            //for setting http header
            $apiKey = $this->viewVars['apiKey'];
            $http = new Client([
                'headers' => ['API_TOKEN' => Security::hash(Configure::read('API_TOKEN'))],
            ]);
            $host = Configure::read('HOST');
            $url = $host . '/api/home';
            //for setting body in the response
            $response = $http->get($url);
            $response = $response->getJson();
            $code = $response['code'];

            if ($code === 200) {
                $posts = $response['data']['Posts'];
                $users = $response['data']['Users'];
                $myProfilePicture = $response['data']['myProfilePicture'];
            }
            // $this->Paginator->settings = $this->paginate;
            // $posts = $this->paginate(['limit' => 10]);
            $this->set('posts', $posts);
            $this->set('users', $users);
            $this->set('myProfilePicture', $myProfilePicture);

            //FOR SAVING POST
            if ($this->request->is('post')) {
                $userPost = $this->request->getData();
                $content = $userPost['content'];
                //FOR CREATING POST TO SAVE LATER IN THE DATABASE WHEN FOUND NO ERROR
                // $postsTable = TableRegistry::getTableLocator()->get('Posts');
                // $post = $postsTable->newEntity();
                // $post->set([
                //     'poster_user_id' => $posterUserId,
                //     'content' => $userPost['content']
                // ]);

                //FOR IMAGE
                $date = '';
                $imgExt = pathinfo($_FILES['picture']['name'], PATHINFO_EXTENSION);
                $isImage = false;
                $validImgExt = ['JPG', 'JPEG', 'GIF', 'JFIF'];
                foreach ($validImgExt as $ext) {
                    if ($ext == strtoupper(($imgExt))) {
                        $isImage = true;
                        $date = date('Ymdhis');
                    }
                }

                if ($isImage || $imgExt === '') {
                    $targetDir = '../webroot/img/uploads/images/';
                    $targetFile = $targetDir . $date . basename($_FILES['picture']['name']);
                    $imageFileType = strtolower(pathinfo($targetFile, PATHINFO_EXTENSION));
                    $filename = $date . basename($_FILES['picture']['name']);
                    // $post->picture = $filename;

                    // $this->Posts->save($post);
                    move_uploaded_file($_FILES['picture']['tmp_name'], $targetFile);

                    //for retrieving
                    $apiKey = $this->viewVars['apiKey'];
                    $token = $this->Auth->user('token');
                    $http = new Client([
                        'headers' => [
                            'X-Api-Key' => $apiKey,
                            'X-Api-Token' => $token
                        ]
                    ]);
                    $host = Configure::read('HOST');
                    $url = 'http://' . $host . '/api/addPost';
                    //for setting body in the response
                    $response = $http->post($url, [
                        'content' => $content,
                        'picture' => $filename
                    ]);
                    $response = $response->getJson();

                    //for checking if the post saving in API is successful through response code
                    $code = $response['code'];
                    if ($code === 200) {
                        $this->Flash->success(__('You have successfully posted.'));

                        return $this->redirect([
                            'controller' => 'posts',
                            'action' => 'index'
                        ]);
                    } else {
                        $this->Flash->error(__('Error while saving your post. Please try again.'));
                    }
                } elseif (!$isImage && $imgExt !== '') {
                    $this->Flash->error(__('File is not an image.'));

                    return $this->redirect([
                        'controller' => 'posts',
                        'action' => 'index'
                    ]);
                }
            }
        }
    }

    /**
     * For viewing specific post
     *
     * @param int $id the id of the post
     *
     * @return CakeResponse|null
     */
    public function view($id = null)
    {
        if (!$this->Auth->user()) {
            $this->redirect(['controller' => 'users', 'action' => 'index']);
        }
        if (!$id) {
            throw new NotFoundException(__('Invalid post'));
        }

        $post = $this->Posts->find('all')
        ->contain([
            'Poster',
            'Likes',
            'Comments',
            'Retweeters',
            'Retweet',
            'RetweetAuthor'
        ])
        ->where(['Posts.id' => $id])
        ->first();

        if (!$post) {
            $this->redirect(['controller' => 'users', 'action' => 'index']);
        }
        $this->loadModel('Users');
        $users = $this->Users->find('all');
        $this->set('users', $users);
        $this->set('post', $post);

        $currentUserId = $this->Auth->user('id');
        $this->loadModel('Users');
        $userProfile = $this->Users->get($currentUserId);
        $this->set('myProfilePicture', $userProfile->profile_picture);

        return null;
    }

    /**
     * for modifying the post
     *
     * @param int $id the post_id of the post
     *
     * @return CakeResponse|null
     */
    public function edit($id = null)
    {
        $lastUrl = $this->referer('/', true);
        if (!$this->Auth->user()) {
            $this->redirect(['controller' => 'users', 'action' => 'index']);
        }
        $dateTimeToday = date('Y-m-d H:i:s');
        if (!$id) {
            $this->Flash->error(__('Post not found.'));

            return $this->redirect(['action' => 'index']);
        }
        $host = Configure::read('HOST');
        $token = $this->Auth->user('token');
        $url = 'http://' . $host . '/api/viewPost';
        $apiKey = $this->viewVars['apiKey'];
            $http = new Client([
                'headers' => [
                    'X-Api-Key' => $apiKey,
                    'X-Api-Token' => $token
                ]
            ]);
        //for setting body in the response
        $response = $http->get($url, [
            'id' => $id
        ]);
        $response = $response->getJson();
        $post = $response['data'];

        $currentUserId = $this->Auth->user('id');
        $this->loadModel('Users');
        $userProfile = $this->Users->get($currentUserId);
        $this->set('myProfilePicture', $userProfile->profile_picture);
        $this->set('post', $post);
        $currentUserId = $this->Auth->user('id');
        if ($post['poster_user_id'] !== $currentUserId) {
            $this->Flash->error(__('You are not allowed to edit this post.'));

            return $this->redirect(['action' => 'index']);
        }
        if (!$id) {
            $this->Flash->error(__('Post not found'));

            return $this->redirect(['action' => 'login']);
        }
        if ($this->request->is('post')) {
            $userPost = $this->request->getData();
            $content = $userPost['content'];
            $update = true;
            //FOR IMAGE
            $imgExt = pathinfo($_FILES['picture']['name'], PATHINFO_EXTENSION);
            $filename = '';
            if ($imgExt !== '') {
                $isImage = false;
                $date = date('Ymdhis');
                $targetDir = '../webroot/img/uploads/images/';
                $targetFile = $targetDir . $date . basename($_FILES['picture']['name']);
                $imageFileType = strtolower(pathinfo($targetFile, PATHINFO_EXTENSION));
                $filename = $date . basename($_FILES['picture']['name']);
                $picture = $filename;
                $validImgExt = ["JPG", "JPEG", "GIF", "JFIF"];
                foreach ($validImgExt as $ext) {
                    if ($ext == strtoupper(($imgExt))) {
                        $isImage = true;
                        move_uploaded_file($_FILES['picture']['tmp_name'], $targetFile);
                    }
                }
                if (!$isImage) {
                    $update = false;
                }
            }
            $url = 'http://' . $host . '/api/editPost';
            //for setting body in the response
            $response = $http->put($url, [
                'id' => $id,
                'content' => $content,
                'picture' => $filename
            ]);
            $response = $response->getJson();
            $code = $response['code'];
            if ($code === 200) {
                return $this->redirect($lastUrl);
            }
        }
    }

    /**
     * For deleting a post. It checks first if the author of the post is the current user
     *
     * @param int $posterId the id of the author
     * @param int $id the id of the post
     *
     * @return CakeResponse|null
     */
    public function delete($posterId, $id)
    {
        if ($this->request->is('ajax')) {
            $currentUserId = $this->Auth->user('id');
            $post = $this->Posts->find()
            ->where(['Posts.id' => $id])
            ->first();
            $posterUserId = $post->poster_user_id;
            if ($posterUserId == $currentUserId) {
                $dateTimeToday = date('Y-m-d H:i:s');
                $post = $this->Posts->newEntity();
                $post = $this->Posts->patchEntity($post, $this->request->getData());
                $post->set([
                    'id' => $id,
                    'deleted' => $dateTimeToday
                ]);
                $this->Posts->save($post);
                $this->set([
                    'my_response' => 'success',
                    '_serialize' => 'my_response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }
        }

        return null;
    }

    /**
     * for retweeting a post
     *
     * @param int $retweetUserId the user_id of the post author
     * @param int $retweetPostId the post_id of the post to be retweeted
     *
     * @return CakeResponse|null
     */
    public function retweet($retweetUserId, $retweetPostId)
    {
        if ($this->request->is('ajax')) {
            $content = $this->request->getQuery('content');
            $dateTimeToday = date('Y-m-d H:i:s');
            $currentUserId = $this->Auth->user('id');
            if ($content === '') {
                $post = $this->Posts->newEntity();
                $post = $this->Posts->patchEntity($post, $this->request->getData());
                $post->set([
                    'poster_user_id' => $currentUserId,
                    'retweet_user_id' => $retweetUserId,
                    'retweet_post_id' => $retweetPostId
                ]);
                $this->Posts->save($post);
            } else {
                $post = $this->Posts->newEntity();
                $post = $this->Posts->patchEntity($post, $this->request->getData());
                $post->set([
                    'poster_user_id' => $currentUserId,
                    'retweet_user_id' => $retweetUserId,
                    'retweet_post_id' => $retweetPostId,
                    'content' => $content
                ]);
                $this->Posts->save($post);
            }
            $this->set([
                'my_response' => 'success',
                '_serialize' => 'my_response',
            ]);
            $this->RequestHandler->renderAs($this, 'json');
        }

        return null;
    }

    /**
     * for viewing all retweets
     *
     * @param int $id the id of the post
     *
     * @return CakeResponse|null
     */
    public function viewRetweet($id)
    {
        $this->set('retweet', $this->Post->find('all'));

        return null;
    }

    public function add($id)
    {
        if ($this->request->is('ajax')) {
            $this->layout = false;
            $username = $this->Auth->user('username');
            $this->set(compact('username'));
        } elseif ($this->request->is('post')) {
            $result = $this->request->getData();
            $data['content'] = $result['content'];
            $data['user_id'] = $id;
            $posts = $this->Api->call('addPost', $data);
            $result = $this->resultParser($posts);
//            if (!is_array($result)) {
//                return $result;
//            }

            return $this->redirect(['controller' => 'pages', 'action' => 'home']);
        }
    }
}
