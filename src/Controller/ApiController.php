<?php

namespace App\Controller;

use Cake\Auth\WeakPasswordHasher;
use Cake\Event\Event;
use Cake\Http\Middleware\BodyParserMiddleware;
use Cake\Utility\Security;

class ApiController extends AppController
{
    public $autoRender = false;

    /**
     * for initialization of the page
     *
     * @return CakeResponse|null
     */
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('ApiResponses');
        $this->loadComponent('Paginator');
        $this->loadComponent('RequestHandler');

        //load models
        $this->loadModel('Users');
    }

    /**
     * for allowing specific pages without needing to login
     *
     * @param int $event checks if the user is allowed to visit specific spaces.
     * Allowed pages are declared in $this->Auth->allow()
     *
     * @return CakeResponse|null
     */
//    public function beforeFilter(Event $event)
//    {
//        $currentDate = date('Y-m-d H:i:s');
//        $usersTable = $this->loadModel('Users');
//        $expireTokens = $usersTable->query();
//        $expireTokens->update()
//        ->set([
//            'is_active' => 0
//        ])
//        ->where(['token_expire <' => $currentDate])
//        ->execute();
//        $response = $this->response;
//        parent::beforeFilter($event);
//
//        //checks if the action exists
//        $apis = [
//            'login',
//            'home',
//            'logout',
//            'register',
//            'viewPost',
//            'addPost',
//            'editPost',
//            'addComment'
//        ];
//        $this->Auth->allow($apis);
//        // for checking if the url is correct
//        if (!in_array($this->request->getParam('action'), $apis)) {
//            $message = __('Invalid URI');
//            $response = $this->ApiResponses->fail(404, $message);
//
//            return $response;
//        }
//
//        //for disabling csrf token for api
//        if (in_array($this->request->getParam('action'), $apis)) {
//            $this->getEventManager()->off($this->Csrf);
//        }
//        $key = $this->request->getHeaderLine('X-Api-Key');
//        $apiKey = $this->viewVars['apiKey'];
//        //for comparing the key in the request matches the original key found in database
//        if ($key !== $apiKey) {
//            $message = __('Unauthorize Access');
//            $response = $this->ApiResponses->fail(401, $message);
//
//            $key = $this->request->getHeaders('X-Api-Key');
//
//            return $response;
//        }
//
//        $restrictActions = [
//            'home',
//            'logout',
//            'viewPost',
//            'addPost',
//            'editPost',
//            'addComment'
//        ];
//        if (in_array($this->request->getParam('action'), $restrictActions)) {
//            //for checking token if still valid
//            $token = $this->request->getHeaderLine('X-Api-Token');
//            if ($token != null) {
//                $currentDate = date('Y-m-d H:i:s');
//                $this->loadModel('Users');
//                $users = $this->Users->find()
//                ->where([
//                    'token' => $token,
//                    'token_expire >= ' => $currentDate,
//                    'is_active' => 1
//                ])
//                ->first();
//                if ($users == null) {
//                    $message = __('Invalid Token');
//                    $response = $this->ApiResponses->fail(401, $message);
//
//                    return $response;
//                } else {
//                    $this->userId = $users['id'];
//                }
//            } else {
//                $message = __('Unauthorize Access/Invalid Token');
//                $response = $this->ApiResponses->fail(401, $message);
//
//                return $response;
//            }
//        }
//
//        return true;
//    }

    /**
     * for logging in api
     *
     * @return response json_encode for the response to the request action
     */
    public function login()
    {
        $response = null;
        if ($this->request->is('post')) {
            $data = $this->request->getData();
            $user = $this->Users->find()
                ->where([
                    'email_address' => $data['email_address'],
                    'password' => Security::hash($data['password'])
                ])
                ->first();

            if ($user) {
                // check token if not expire or is active
                $token = $this->checkAndGenerateAccessToken($user);
                try {
                    $result = $this->Users->save($user);
                    $result['token'] = $token->key;
                    $result['token_expire'] = $token->expiration->format('Y-m-d H:i:s');
                } catch (\PDOException $e) {
                    // $message = $e->getMessage();
                    $message = __('Internal Server Error');
                    return $this->ApiResponses->fail(500, $message);
                }

                $message = __('Login Success');
                $response = $this->ApiResponses->success($message, $result);
            } else {
                $message = __('Invalid Username or Password. Try Again!');
                $response = $this->ApiResponses->fail(401, $message);
            }
        } else {
            $message = __('Request Method Not Allowed');
            $response = $this->ApiResponses->fail(405, $message);
        }

        return $response;
    }

    /**
     * for the homepage of the. All posts that the user follows will appear here.
     *
     * @return array response the json encoded data of all posts
     */
    public function home()
    {
        if ($this->request->is('get')) {
            $response = $this->response;
            $currentDate = date('Y-m-d H:i:s');

            $data = null;
            $message = __('You can see posts');

            $this->loadModel('Follows');
            $follows = $this->Follows->find('list', [
                'valueField' => 'following_id',
                'conditions' => ['Follows.follower_id' => $userId]
            ])
            ->toArray();
            $follows[] = $userId;

            $this->loadModel('Users');
            $users = $this->Users->find('all');
            $users = $users->toArray();

            $this->loadModel('Comments');
            $this->loadModel('Posts');
            $posts = $this->Posts->find('all')
            ->contain([
                'Poster',
                'Likes',
                'Comments',
                'Retweeters',
                'Retweet',
                'RetweetAuthor'
            ])
            ->order(['Posts.modified DESC'])
            ->where([
                ['Posts.deleted IS null'],
                ["Posts.poster_user_id IN " => $follows]
            ]);
            $this->Paginator->settings = $this->paginate;
            $posts = $this->paginate($posts, ['limit' => 10]);

            //for getting the account details of the user
            $this->loadModel('Users');
            $currentUserId = $this->userId;
            $users = $this->Users->find('all')
            ->toArray();
            $userProfile = $this->Users->get($currentUserId);

            // $this->loadModel('Follows');
            // $follows = $this->Follows->find('list', [
            //     'valueField' => 'following_id',
            //     'conditions' => ['Follows.follower_id' => $userId]
            // ])
            // ->toArray();
            // $follows[] = $userId;

            // $this->loadModel('Posts');
            // $posts = $this->Posts->find('all')
            // ->contain([
            //     'Poster'
            // ])
            // ->select([
            //     'Posts.id',
            //     'Posts.content',
            //     'Posts.picture',
            //     'Posts.created',
            //     'Poster.id',
            //     'Poster.firstname',
            //     'Poster.lastname',
            //     'Poster.profile_picture'
            // ])
            // ->order(['Posts.modified DESC'])
            // ->where([
            //     ['Posts.deleted IS null'],
            //     ["Posts.poster_user_id IN " => $follows]
            // ])
            // ->toArray();

            $data['Posts'] = $posts;
            $data['Users'] = $users;
            $data['myProfilePicture'] = $userProfile->profile_picture;
            $response = $this->ApiResponses->success($message, $data);
        } else {
            $message = __('Request Method Not Allowed');
            $response = $this->ApiResponses->fail(405, $message);
        }

        return $response;
    }

    /**
     * for logging out the user. Is_active field in the database will also be set to 0 which means that the user has been logged out.
     *
     * @return array response the json encode informing that the user has successfully logged out.
     */
    public function logout($id)
    {
        if ($this->request->is('delete')) {
            $valid = $this->validateToken($id);
            if ($valid === true) {
                $message = __('Logout success');
                $this->loadModel('Tokens');
                $logoutQuery = $this->Tokens->query()
                    ->update()
                    ->set([
                        'deleted' => 1
                    ])
                    ->where(['key' => $this->request->getHeaderLine('X-API-Key')])
                    ->execute();

                return $this->ApiResponses->success($message);
            }

            $message = 'Access Forbidden';
            $response = $this->ApiResponses->fail(403, $message);
        } else {
            $message = 'Request Method Not Allowed';
            $response = $this->ApiResponses->fail(405, $message);
        }

        return $response;
    }

    /**
     * registration of a new user for api
     *
     * @return json response the json_encode checks whether the registration is successful
     */
    public function register()
    {
        if ($this->request->is('post')) {
            $this->loadModel('Users');
            $data = $this->request->getData();
            $user = $this->Users->newEntity($data);
            // validatation errors
            if ($user->hasErrors()) {
                $message = __('Error in registration');
                return $this->ApiResponses->validationErrors($user->errors());
            }

            $date = date("mdy");
            $length = 6;
            $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $randomString = '';
            for ($ctr = 1; $ctr < $length; $ctr++) {
                $randomString .= $characters[rand(0, strlen($characters) - 1)];
            }
            $activationId = md5($randomString);

            //END OF GENERATING RANDOM STRING
            $emailAddress = $user['email_address'];
            $fullname = $user['firstname'] . ' ' . $user['lastname'];
            $user->set([
                'activation' => $activationId,
            ]);
            // save
            if (!$this->Users->save($user)) {
                $message = __('Internal Server Error');
                $response = $this->ApiResponses->fail(500, $message);
            }

            $message = __('Registration Success');
            $response = $this->ApiResponses->success($message);
        } else {
            $message = __('Request Method Not Allowed');
            $response = $this->ApiResponses->fail(405, $message);
        }

        return $response;
    }

    public function timeline()
    {
        $this->loadModel('Posts');
        $user = $this->request->getQueryParams();
        // check access token
        $validate = $this->validateToken($user['id'] ?? null);
        if ($validate !== true) {
            return $validate;
        }

        $posts = $this->Posts->find()
                ->contain([
                    'Users' => [
                        'conditions' => ['Users.deleted' => 0]
                    ],
                    'Likes' => [
                        'conditions' => ['Likes.deleted' => 0]
                    ],
                    'ChildPosts' => [
                        'conditions' => ['ChildPosts.deleted' => 0]
                    ],
                ])
                ->where([
                    'Posts.user_id' => $user['id'],
                    'Posts.deleted' => 0,
                ])
                ->order(['Posts.created' => 'DESC'])
                ->offset($user['offset'] ?? 0)
                ->limit($user['limit'] ?? 20)
                ->all();

        $message = __('SUCCESS');
        $response = $this->ApiResponses->success($message, $posts);

        return $response;
    }

    /**
     * For adding a new post the current user who is logged in
     *
     * @return json $response the json encoded data for informing if adding of the post is successful or not
     */
    public function addPost()
    {
        if ($this->request->is('post')) {
            $request = $this->request->getData();
            $id = $request['user_id'] ?? null;
            $valid = $this->validateToken($id);
            if (!is_bool($valid)) {
                return $valid;
            }

            $this->loadModel('Posts');
            $post = $this->Posts->newEntity($request);
            $message = __('Successfully added new posts!');
            // for post saving
            if ($this->Posts->save($post)) {
                return $this->ApiResponses->success($message);
            } else {
                $message = __('Error while saving your post');
                $response = $this->ApiResponses->fail(500, $message);
            }
        } else {
            $message = __('Request Method Not Allowed');
            $response = $this->ApiResponses->fail(405, $message);
        }

        return $response;
    }

    /**
     * For adding a new comment to a post
     *
     * @return json $response the json encoded data for informing if adding of the comment is successful or not
     */
    public function addComment()
    {
        if ($this->request->is('post')) {
            $this->loadModel('Comments');
            $currentUserId = $this->userId;
            $commentInfo = $this->request->getData();
            $commentContent = $commentInfo['commentContent'];
            $id = $commentInfo['post_id'];
            // $commentContent = $this->request->getQuery('commentContent');
            $comment = $this->Comments->newEntity();
            $comment->set([
                'user_id' => $currentUserId,
                'post_id' => $id,
                'comment' => $commentContent
            ]);
            if ($this->Comments->save($comment)) {
                $message = __('Comment successfuly saved');
                $response = $this->ApiResponses->success($message);
            } else {
                $message = __('Error while saving the comment');
                $response = $this->ApiResponses->fail(500, $message);
            }
        } else {
            $message = __('Request Method Not Allowed');
            $response = $this->ApiResponses->fail(405, $message);
        }

        return $response;
    }

    /**
     * for editing a post through API
     *
     * @return json $response - the json_encoded data for responses
     */
    public function editPost()
    {
        if ($this->request->is('put')) {
            $postInfo = $this->request->getData();
            $id = $postInfo['id'];
            $content = $postInfo['content'];
            $picture = $postInfo['picture'];
            $this->loadModel('Posts');
            $savePost = $this->Posts->get($id);
            $savePost->set([
                'id' => $id,
                'content' => $content,
                'picture' => $picture
            ]);
            $message = __('Post modified');
            $response = $this->ApiResponses->success($message);
            $this->Posts->save($savePost);
        } else {
            $message = __('Request Method Not Allowed');
            $response = $this->ApiResponses->fail(405, $message);
        }

        return $response;
    }

    /**
     * for viewing a post through API
     *
     * @return json $response - the json_encoded post data
     */
    public function viewPost()
    {
        if ($this->request->is('get')) {
            $this->loadModel('Posts');
            $id = $this->request->getQuery('id');
            $post = $this->Posts->find('all')
            ->contain([
                'Poster',
                'Likes',
                'Comments',
                'Retweeters',
                'Retweet',
                'RetweetAuthor'
            ])
            ->where(['Posts.id' => $id])
            ->first();
            $message = __('Post found');
            $response = $this->ApiResponses->success($message, $post);
        } else {
            $message = __('Request Method Not Allowed');
            $response = $this->ApiResponses->fail(405, $message);
        }

        return $response;
    }


    public function checkAndGenerateAccessToken($user)
    {
        $this->loadModel('Tokens');
        $token = $this->Tokens->find()
                ->where([
                    'user_id' => $user->id,
                    'deleted' => 0
                ])->first();

        if (is_null($token) || $token->expiration->format('Y-m-d H:i:s') <= date('Y-m-d H:i:s')) {
            //for producing a token
            $date = date('mdy');
            $tokenExpiration = date('Y-m-d H:i:s', strtotime('+124 hours'));
            $length = 6;
            $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $randomString = '';
            for ($ctr = 1; $ctr < $length; $ctr++) {
                $randomString .= $characters[rand(0, strlen($characters) - 1)];
            }

            $data = [
                'key' => Security::hash($randomString . $user->id),
                'user_id' => $user->id,
                'expiration' => $tokenExpiration
            ];

            $entity = $this->Tokens->newEntity($data);
            $token = $this->Tokens->save($entity);
        }

        return $token;
    }

    public function validateToken($user)
    {
        $this->loadModel('Tokens');
        $accessToken = $this->request->getHeaderLine('X-API-Key');
        $token = $this->Tokens->find()
                    ->where([
                        'user_id' => $user,
                        'deleted' => 0,
                        'key' => $accessToken
                    ])
                    ->first();

        if (is_null($token) || $token->expiration->format('Y-m-d H:i:s') <= date('Y-m-d H:i:s')) {
            $message = __('Access Forbidden');
            return $this->ApiResponses->fail('403', $message);
        }

        return true;
    }
}
