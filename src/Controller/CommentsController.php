<?php
namespace App\Controller;

use App\Controller\WebController;
use Cake\Http\Client;

class CommentsController extends WebController
{

    public $components = ['RequestHandler'];

    /**
     * Checks if the user is authorize to perform an action
     *
     * @param int $user checks if the current user id is authorized to perform an action
     *
     * @return bool
     */
    public function isAuthorized($user)
    {
        // All registered users can add posts
        $allowedActions = ['addComment', 'deleteComment', 'editComment', 'delete'];
        if (in_array($this->request->getParam('action'), $allowedActions)) {
            return true;
        }

        return parent::isAuthorized($user);
    }

    /**
     * for adding a comment to a post
     *
     * @param int $id the id of the post to be commented
     *
     * @return json the json_encoded response from the api
     */
    public function addComment($id)
    {
        // $dateTimeToday = date('Y-m-d H:i:s');
        // $currentUserId = $this->Auth->user('id');

        if ($this->request->is('ajax')) {
            $commentContent = $this->request->getQuery('commentContent');
            $host = $this->viewVars['host'];
            $url = 'http://' . $host . '/api/addComment/' . $id;
            //for api retrieving
            $apiKey = $this->viewVars['apiKey'];
            $token = $this->Auth->user('token');
            //for setting header
            $http = new Client([
                'headers' => [
                    'X-Api-Key' => $apiKey,
                    'X-Api-Token' => $token
                ]
            ]);
            //for setting body in the response
            $response = $http->post($url, [
                'post_id' => $id,
                'commentContent' => $commentContent
            ]);
            $response = $response->getStringBody();
            $code = $response['code'];
            $this->set([
                'response' => $response,
                '_serialize' => 'response',
            ]);

            return $this->RequestHandler->renderAs($this, 'json');
        }
    }

    /**
     * for deleting a post
     *
     * @param int $id the id of the comment to be deleted
     *
     * @return CakeResponse|null
     */
    public function deleteComment($id)
    {
        $comment = $this->Comment->find('first', [
            'conditions' => ['Comment.id' => $id]
        ]);
        $userId = $post['Comment']['user_id'];
        if ($userId === $this->Auth->user('id')) {
            $this->Comments->set([
                'id' => $id,
                'deleted' => $dateTimeToday,
            ]);
            $this->Comment->save();
        }

        return null;
    }

    /**
     * for editing or modifying a comment
     *
     * @param int $id the id of the comment to be modified
     *
     * @return CakeResponse|null
     */
    public function editComment($id)
    {
        if (!$this->Auth->user()) {
            $this->redirect(['controller' => 'users', 'action' => 'index']);
        }
        if (!$id) {
            throw new NotFoundException(__('Invalid Post'));
        }
        if ($this->request->is('ajax')) {
            $comment = $this->Comments->findById($id)
            ->first();
            $currentUserId = $this->Auth->user('id');
            if ($comment->user_id !== $currentUserId) {
                $this->Flash->error(__('You are not allowed to edit this comment.'));

                return $this->redirect(['controller' => 'posts', 'action' => 'index']);
            }
            $updateComment = $this->request->getData();
            $newComment = $this->request->getQuery('commentContent');
            $dateTimeToday = date('Y-m-d H:i:s');
            $comment = $this->Comments->newEntity();
            $comment = $this->Comments->patchEntity($comment, $this->request->getData());
            $comment->set([
                'id' => $id,
                'comment' => $newComment
            ]);
            $this->Comments->save($comment);
            $this->set([
                'my_response' => 'success',
                '_serialize' => 'my_response',
            ]);
            $this->RequestHandler->renderAs($this, 'json');
        }
    }

    /**
     * for deleting a comment to a post
     *
     * @param int $id the id of the comment to be deleted
     *
     * @return CakeResponse|null
     */
    public function delete($id)
    {
        if (!$this->Auth->user()) {
            $this->redirect(['controller' => 'users', 'action' => 'index']);
        }
        if (!$id) {
            throw new NotFoundException(__('Invalid Post'));
        }
        if ($this->request->is('ajax')) {
            $comment = $this->Comments->findById($id)
            ->first();
            $currentUserId = $this->Auth->user('id');
            if ($comment->user_id !== $currentUserId) {
                $this->Flash->error(__('You are not allowed to delete this comment.'));

                return $this->redirect(['controller' => 'posts', 'action' => 'index']);
            }
            $dateTimeToday = date('Y-m-d H:i:s');
            $comment = $this->Comments->newEntity();
            $comment = $this->Comments->patchEntity($comment, $this->request->getData());
            $comment->id = $id;
            $comment->deleted = $dateTimeToday;
            $this->Comments->save($comment);
            $this->set([
                'my_response' => 'success',
                '_serialize' => 'my_response',
            ]);
            $this->RequestHandler->renderAs($this, 'json');
        }
    }
}
