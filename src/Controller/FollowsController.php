<?php
namespace App\Controller;

use App\Controller\WebController;

class FollowsController extends WebController
{

    /**
     * Checks if the user is authorize to perform an action
     *
     * @param int $user checks if the current user id is authorized to perform an action
     *
     * @return bool
     */
    public function isAuthorized($user)
    {
        // ALL REGISTERED USERS CAN FOLLOW AND UNFOLLOW ANOTHER USERS
        $allowedActions = ['follow', 'unfollow'];
        if (in_array($this->request->getParam('action'), $allowedActions)) {
            return true;
        }

        return parent::isAuthorized($user);
    }

    /**
     * For unfollowing other user
     *
     * @param int $id the follow id
     * @param int $userId the user id of the user who you want to unfollow
     *
     * @return CakeResponse|null
     */
    public function unfollow($id, $userId)
    {
        if (!$id) {
            throw new NotFoundException(__('Invalid User'));
        }
        $currentUserId = $this->Auth->user('id');
        $follow = $this->Follows->find()
        ->where([
            'Follows.id ' => $id,
            'Follows.follower_id' => $currentUserId
        ])
        ->first();
        if ($follow !== null) {
            $followId = $follow->id;
            $entity = $this->Follows->get($followId);
            $this->Follows->delete($entity);
            $this->Flash->success(__('You just unfollowed a user.'));
        } else {
            $this->Flash->error(__('You are not not allowed to perform this action.'));
        }

        return $this->redirect(['controller' => 'users', 'action' => 'view', $userId, '#' => 'following']);
    }

    /**
     * for following another user
     *
     * @param int $id the user id of the user to be follow id
     *
     * @return CakeResponse|null
     */
    public function follow($id)
    {
        if (!$id) {
            throw new NotFoundException(__('Invalid User'));
        }
        if (!$this->Auth->user()) {
            return $this->redirect(['controller' => 'users', 'action' => 'index']);
        } else {
            $currentUserId = $this->Auth->user('id');
            $follow = $this->Follows->newEntity();
            $follow = $this->Follows->patchEntity($follow, $this->request->getData());
            $follow->set([
                'follower_id' => $currentUserId,
                'following_id' => $id
            ]);
            if ($this->Follows->save($follow)) {
                $this->Flash->success(__('You just followed this user.'));

                return $this->redirect(['controller' => 'users', 'action' => 'view', $id]);
            }
        }
    }
}
