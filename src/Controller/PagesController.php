<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Http\Client;
use Cake\Http\Exception\ForbiddenException;
use Cake\Http\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Utility\Security;
use App\Controller\WebController;

/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link https://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class PagesController extends WebController
{
    public $components = ['Api'];
    /**
     * Displays a view
     *
     * @param array ...$path Path segments.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Http\Exception\ForbiddenException When a directory traversal attempt.
     * @throws \Cake\Http\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
    public function home()
    {
        if (!$this->Auth->user()) {
            return $this->redirect(['controller' => 'users', 'action' => 'login']);
        } else {
            $currentUserId = $this->Auth->user('id');
            $token = $this->Auth->user('token');
            $postOption = [
                'id' => $currentUserId,
                'offset' => 0,
                'limit' => 20
            ];
            //for setting body in the response
            $posts = $this->Api->call('getPosts', $postOption);
            $result = $this->resultParser($posts, false);
            if (!is_array($result)) {
                return $result;
            }
            $likes = $followers = $follows = $shared = 0;
            // output data
            $this->set('posts', $result);
            $this->set('likes', $likes);
            $this->set('followers', $followers);
            $this->set('follows', $follows);
            $this->set('shared', $shared);
            $this->set('album', []);
            $this->set('trends', []);
        }
    }
}
