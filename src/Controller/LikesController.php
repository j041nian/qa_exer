<?php
namespace App\Controller;

use App\Controller\WebController;

class LikesController extends WebController
{
    /**
     * Checks if the user is authorize to perform an action
     *
     * @param int $user checks if the current user id is authorized to perform an action
     *
     * @return bool
     */
    public function isAuthorized($user)
    {
        // ALL REGISTERED USERS CAN FOLLOW ANOTHER USERS
        $allowedActions = ['like', 'unlike'];
        if (in_array($this->request->getParam('action'), $allowedActions)) {
            return true;
        }

        return parent::isAuthorized($user);
    }

    /**
     * for liking a post
     *
     * @param int $id the id of the specific post to be liked by the current user
     *
     * @return CakeResponse|null
     */
    public function like($id)
    {
        if ($this->request->is('ajax')) {
            if (!$id) {
                $this->Flash->error(__('Post not found.'));
            }
            $currentUserId = $this->Auth->user('id');
            $like = $this->Likes->newEntity();
            $like = $this->Likes->patchEntity($like, $this->request->getData());
            $like->set([
                'liker_user_id' => $currentUserId,
                'post_id' => $id
            ]);
            $response = $this->Likes->save($like);
            $this->set([
                'my_response' => 'success',
                '_serialize' => 'my_response',
            ]);
            $this->RequestHandler->renderAs($this, 'json');
        }

        return null;
    }

    /**
     * for unliking a post
     *
     * @param int $id the like_id of the specific post to be unliked
     *
     * @return CakeResponse|null
     */
    public function unlike($id)
    {
        if ($this->request->is('ajax')) {
            $currentUserId = $this->Auth->user('id');
            $like = $this->Likes->find()
            ->where([
                'Likes.post_id ' => $id,
                'likes.liker_user_id' => $currentUserId
            ])
            ->first()
            ->toArray();
            if (count($like) > 0) {
                $likeId = $like['id'];
                $entity = $this->Likes->get($likeId);
                $this->Likes->delete($entity);
                $this->set([
                    'my_response' => 'success',
                    '_serialize' => 'my_response',
                ]);
                $this->RequestHandler->renderAs($this, 'json');
            }
        }

        return null;
    }
}
