<?php
namespace App\Controller;

use App\Controller\WebController;
use App\Form\UsersForm;
use Cake\Core\Configure;
use Cake\Event\Event;
//use Cake\Http\Client;
//use Cake\Http\Middleware\BodyParserMiddleware;
//use Cake\Mailer\Email;
//use Cake\Mailer\TransportFactory;
//use Cake\Utility\Security;

class UsersController extends WebController
{
    /**
     * for user login
     *
     * @return CakeResponse|null
     */
    public function login()
    {
        $this->layout = 'custom';
        if ($this->Auth->user()) {
            $this->redirect(['controller' => 'pages', 'action' => 'home']);
        }

        if ($this->request->is('post')) {
            $user = $this->request->getData();
            $response = $this->Api->call('login', $user);
            $result = $this->resultParser($response, false);
            if (!is_array($result)) {
                return $result;
            }

            $token = $result['token'];
            $user = $result;
            $this->Auth->setUser($user);

            return $this->redirect($this->Auth->redirectUrl());
        }
    }

    /**
     * for registration of the new user
     *
     * @return CakeResponse|null
     */
    public function register()
    {
        $this->layout = 'custom';
        $user = new UsersForm();
        $this->set(compact('user'));
        if ($this->request->is('post')) {
            // FOR GETTING URL
            $data = $this->request->getData();
            $response = $this->Api->call('register', $data)->getJson();

            switch($response['code']) {
                case 200:
                    $this->Flash->success(__('You are now registered. Please check your email.'));
                    return $this->redirect(['action' => 'login']);
                // invalid parameters
                case 409:
                    foreach ($response['data'] as $error) {
                        $validation[$error['field']] = [$error['message']];
                    }
                    $user->setErrors($validation);
                    return $this->Flash->error(__('Error Saving Data! Validation Error Occured!'));
            }
        }
    }

    /**
     * for logging out a user
     *
     * @return CakeResponse|null
     */
    public function logout()
    {
        $this->autoRender = false;
        $response = $this->Api->call('logout', $this->Auth->user('id'));
        $result = $this->resultParser($response, false);
        if (!is_array($result)) {
            return $result;
        }

        $message = __('Your have been logged out successfully');
        $this->Flash->success($message);
        return $this->redirect($this->Auth->logout());
    }

    /**
     * For confirming an account. The link sent to the email must be clicked in order for the user
     * to confirm his account.
     *
     * @param string $activationId random string sent to the email
     *
     * @return CakeResponse|null
     */
    public function confirm($activationId)
    {
        // $id =  $this->request->params['id'];
        $validateUser = $this->Users->find('all')
        ->where(['Users.is_confirmed = 0 AND activation_id = "' . $activationId . '"'])
        ->first();
        if ($validateUser !== null) {
            $id = $validateUser->id;
            $user = $this->Users->findById($id)
            ->first();

            $user = $this->Users->newEntity();
            $user = $this->Users->patchEntity($user, $this->request->getData());
            $user->set([
                'id' => $id,
                'is_confirmed' => 1
            ]);

            if ($this->Users->save($user)) {
                $this->Flash->success(__('You have just confirmed your account. Enjoy! Please login.'));
                $this->redirect(['controller' => 'users', 'action' => 'login']);
            }
        } else {
            $this->Flash->error(__('Invalid'));

            return $this->redirect(['action' => 'index']);
        }
    }

    /**
     * for the profile page of the current user
     *
     * @return CakeResponse|null
     */
    public function profile()
    {
        if (!$this->Auth->user()) {
            $this->redirect(['controller' => 'users', 'action' => 'index']);
        }
        $currentUserId = $this->Auth->user('id');
        $userProfile = $this->Users->get($currentUserId);
        $this->set('fullname', ucwords($userProfile->firstname . ' ' . $userProfile->lastname));
        $this->set('myProfilePicture', $userProfile->profile_picture);
        $dateTimeToday = date('Y-m-d H:i:s');
        $this->loadModel('Posts');

        //FOR PAGINATION
        // $this->Paginator->settings = $this->paginate;
        $this->Paginator->settings = [
            'conditions' => ['Post.poster_user_id' => $currentUserId],
            'order' => 'Post.created DESC',
            'limit' => 10
        ];
        // $posts = $this->Paginator->paginate('Post');
        $posts = $this->Posts->find('all')
        ->contain([
            'Poster',
            'Likes',
            'Comments',
            'Retweeters',
            'Retweet',
            'RetweetAuthor'
        ])
        ->limit(5)
        ->order(['Posts.modified DESC'])
        ->where(['Posts.deleted IS null AND Posts.poster_user_id = ' . $currentUserId]);
        // $posts = $posts->toArray();
        $posts = $this->paginate($posts, ['limit' => 10]);
        $this->set('posts', $posts);
        $this->set('showCreate', true);
        //END OF PAGINATION

        $this->loadModel('Follows');
        //FOR FOLLOWERS
        $followers = $this->Follows->find('all')
        ->contain([
            'Follower'
        ])
        ->where('Follows.following_id = ' . $currentUserId)
        ->toArray();

        //FOR FOLLOWING
        $followings = $this->Follows->find('all')
        ->contain([
            'Following'
        ])
        ->where('Follows.follower_id = ' . $currentUserId)
        ->toArray();

        $this->set('followers', $followers);
        $this->set('followings', $followings);
        $this->set('profile', $this->Users->findById($currentUserId));
        $this->set('users', $this->Users->find('all'));

        $posterUserId = $currentUserId;
        //FOR SAVING POST
        $userPost = $this->request->getData();
        if ($this->request->is('post')) {
            //FOR CREATING POST TO SAVE LATER IN THE DATABASE WHEN FOUND NO ERROR
            $post = $this->Posts->newEntity();
            $post = $this->Posts->patchEntity($post, $this->request->getData());
            $post->poster_user_id = $posterUserId;
            //FOR IMAGE
            $date = '';
            $imgExt = pathinfo($_FILES['picture']['name'], PATHINFO_EXTENSION);
            $isImage = false;
            $validImgExt = ['JPG', 'JPEG', 'GIF', 'JFIF'];
            foreach ($validImgExt as $ext) {
                if ($ext == strtoupper(($imgExt))) {
                    $isImage = true;
                    $date = date('Ymdhis');
                }
            }
            $targetDir = '../webroot/img/uploads/images/';
            $targetFile = $targetDir . $date . basename($_FILES['picture']['name']);
            $imageFileType = strtolower(pathinfo($targetFile, PATHINFO_EXTENSION));
            $filename = $date . basename($_FILES['picture']['name']);
            $post->picture = $filename;

            if ($isImage || $imgExt === '') {
                $this->Posts->save($post);
                move_uploaded_file($_FILES['picture']['tmp_name'], $targetFile);
                $this->Flash->success(__('You have successfully posted.'));

                return $this->redirect([
                    'controller' => 'users',
                    'action' => 'profile'
                ]);
            } elseif (!$isImage && $imgExt !== '') {
                $this->Flash->error(__('File is not an image.'));

                return $this->redirect([
                    'controller' => 'users',
                    'action' => 'profile'
                ]);
            }
            // $this->Flash->error(__('Unable to add your post'));
        }
    }

    /**
     * for viewing another user
     *
     * @param int $id the user_id of another user
     *
     * @return CakeResponse|null
     */
    public function view($id = null)
    {
        if (!$this->Auth->user()) {
            $this->redirect(['controller' => 'users', 'action' => 'index']);
        }
        $dateTimeToday = date('Y-m-d H:i:s');
        if (!$id) {
            throw new NotFoundException(__('Invalid User'));
        }
        $currentUserId = $this->Auth->user('id');
        if ($currentUserId == $id) {
            return $this->redirect(['action' => 'profile']);
        }

        $user = $this->Users->findById($id)
        ->first()
        ->toArray();

        $this->loadModel('Posts');
        $posts = $this->Posts->find('all')
        ->contain([
            'Poster',
            'Likes',
            'Comments',
            'Retweeters',
            'Retweet',
            'RetweetAuthor'
        ])
        ->limit(5)
        ->order(['Posts.modified DESC'])
        ->where(['Posts.deleted IS null AND Posts.poster_user_id = ' . $id]);
        $posts = $posts->toArray();
        $this->loadModel('Follows');
        //FOR FOLLOWERS
        $followers = $this->Follows->find('all')
        ->contain([
            'Follower'
        ])
        ->where('Follows.following_id = ' . $id)
        ->toArray();

        //FOR FOLLOWING
        $followings = $this->Follows->find('all')
        ->contain([
            'Following'
        ])
        ->where('Follows.follower_id = ' . $id)
        ->toArray();

        $this->set('followings', $followings);
        $this->set('followers', $followers);
        $this->set('posts', $posts);
        $this->set('user', $user);
        $this->set('showCreate', false);
        $this->set('users', $this->Users->find('all'));
    }

    /**
     * for modifiyfing current user information
     *
     * @return CakeResponse|null
     */
    public function edit()
    {
        if (!$this->Auth->user()) {
            $this->redirect(['controller' => 'users', 'action' => 'index']);
        }
        $currentUserId = $this->Auth->user('id');
        $userProfile = $this->Users->get($currentUserId);
        $this->set('fullname', ucwords($userProfile->firstname . ' ' . $userProfile->lastname));
        $this->set('myProfilePicture', $userProfile->profile_picture);
        $user = $this->Users->findById($currentUserId)
        ->first();
        $this->set('user', $user);
        if ($this->request->is(['post', 'put'])) {
            $user = $this->Users->newEntity();
            $user = $this->Users->patchEntity($user, $this->request->getData());
            $user->id = $currentUserId;
            $update = true;
            $updateData = $this->request->getData();
            if (isset($updateData['newPassword'])) {
                $newPassword = $updateData['newPassword'];
                $retypePassword = $updateData['retypeNewPassword'];

                //CHECKS IF BOTH PASSWORD MATCHES
                if ($newPassword === $retypePassword) {
                    $user->password = $newPassword;
                    $this->Users->save($user);
                    $this->Flash->success(__('Password successfully changed.'));

                    return $this->redirect(['action' => 'edit', '#' => 'accountSettings']);
                } else {
                    $this->Flash->error(__('Passwords do not match.'));
                }
            } else {
                $filename = '';
                //FOR IMAGE
                $imgExt = pathinfo($_FILES['picture']['name'], PATHINFO_EXTENSION);
                if ($imgExt !== '') {
                    $isImage = false;
                    $date = date('Ymdhis');
                    $targetDir = '../webroot/img/uploads/images/';
                    $targetFile = $targetDir . $date . basename($_FILES['picture']['name']);
                    $imageFileType = strtolower(pathinfo($targetFile, PATHINFO_EXTENSION));
                    $filename = '';
                    $validImgExt = ['JPG', 'JPEG', 'GIF', 'JFIF'];
                    foreach ($validImgExt as $ext) {
                        if ($ext == strtoupper(($imgExt))) {
                            $isImage = true;
                            $filename = $date . basename($_FILES['picture']['name']);
                            move_uploaded_file($_FILES['picture']['tmp_name'], $targetFile);
                            $user->profile_picture = $filename;
                        }
                    }
                    if (!$isImage) {
                        $update = false;
                    }
                }
                if ($update) {
                    $this->Users->save($user);
                    $this->Flash->success(__('You have successfully updated your profile'));
                    $this->redirect(['action' => 'edit', '#' => 'personalInformation']);
                }
            }
        }
    }

    /**
     * for searching other users and posts
     *
     * @return CakeResponse|null
     */
    public function search()
    {
        if (!$this->Auth->user()) {
            $this->redirect(['controller' => 'users', 'action' => 'index']);
        }
        if ($this->request->is(['post', 'put'])) {
            $search = $this->request->getData();
            $keyword = $search['keyword'];
            if ($keyword === '') {
                $this->Flash->error(__('You did not type anything'));

                return $this->redirect(['action' => 'index']);
            }

            //FOR SEARCHING USERS
            $allUsers = $this->Users->find('all')
            ->where(['OR' => [
                ['firstname LIKE ' => '%' . $keyword . '%'],
                ['lastname LIKE ' => '%' . $keyword . '%'],
                ["CONCAT(firstname,' ', lastname) LIKE " => '%' . $keyword . '%'],
            ]])
            ->toArray();

            //FOR SEARCHING POSTS
            $this->loadModel('Posts');
            $allPosts = $this->Posts->find('all')
            ->contain([
                'Poster',
                'Likes',
                'Retweeters',
                'Retweet',
                'RetweetAuthor'
            ])
            ->where([
                ['Posts.content LIKE ' => '%' . $keyword . '%'],
                ['Posts.deleted IS null']
            ])
            ->order('Posts.modified DESC')
            ->toArray();
            $this->set('posts', $allPosts);
            $this->set('allUsers', $allUsers);
            $this->set('keyword', $keyword);
        } else {
            $this->redirect(['controller' => 'posts', 'action' => 'index']);
        }
    }
}
