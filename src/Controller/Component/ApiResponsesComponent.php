<?php

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Http;

class ApiResponsesComponent extends Component
{
    public $components = ['RequestHandler'];

    /**
     * For executing response for the api.
     *
     * @param array $config the configured parameter for initialization the the response component
     *
     * @return CakeResponse|null
     * */
    public function initialize(array $config)
    {
        $this->response;

        return null;
    }

    /**
     * this method is called when the api request is success
     *
     * @param string $message response message for api
     * @param array $data the data to be displayed on the response for api
     *
     * @return array $response with the type and message to inform the user
     */
    public function success($message, $data = null)
    {
        $response = $this->response;
        $response = $response->withType('application/json')
        ->withStringBody(json_encode([
            'type' => 'success',
            'code' => 200,
            'message' => $message,
            'data' => $data
        ]));

        return $response;
    }

    /**
     * this function is called if there is error in the request
     *
     * @param int $errorCode the code of the error
     * @param string $message the message to inform the user and what type of error did he commited.
     *
     * @return array $response the json encoded for the response to the request
     */
    public function fail($errorCode, $message)
    {
        $response = $this->response;
        $response = $response->withType('application/json')
        ->withStringBody(json_encode([
            'type' => 'fail',
            'code' => $errorCode,
            'message' => $message,
            'data' => null
        ]));

        return $response;
    }

    public function validationErrors($errors)
    {
        $code = 409;
        $return = [];
        foreach ($errors as $field => $value) {
            $return[] = [
                'field' => $field,
                'message' => implode('. ', $value)
            ];
        }

        $response = $this->response;
        $response = $response->withType('application/json')
        ->withStringBody(json_encode([
            'type' => 'fail',
            'code' => $code,
            'message' => 'Invalid Parameters',
            'data' => $return
        ]));

        return $response;
    }

}
