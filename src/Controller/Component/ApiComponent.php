<?php
namespace App\Controller\Component;

use Cake\Core\Configure;
use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Cake\Http\Client;
use Cake\Utility\Security;

/**
 * ApiCall component
 */
class ApiComponent extends Component
{
    public $components = ['RequestHandler', 'Auth'];
    public $http;
    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [];

    public function initialize($config)
    {
        parent::initialize($config);
        $this->http = new Client([
            'headers' => [
                'X-API-Token' => Security::hash(Configure::read('API_TOKEN')),
                'X-API-Key' => $this->Auth->user('token') ?? null
            ],
        ]);
    }

    public function call($name, $options)
    {
        try {
            $response = $this->{$name}($options);
        } catch (\Exception $e) {
            $response = $e->getMessage();
        }

        return $response;
    }

    public function register($options)
    {
        $host = Configure::read('HOST');
        $url = $host . '/api/register';
        $response = $this->http->post($url, $options);

        return $response;
    }

    public function login($options)
    {
        $host = Configure::read('HOST');
        $url = $host . '/api/login';
        $response = $this->http->post($url, $options);

        return $response;
    }

    public function logout($id)
    {
        $host = Configure::read('HOST');
        $url = $host . '/api/logout/' . $id;
        $response = $this->http->delete($url);

        return $response;
    }

    public function getPosts(array $options)
    {
        $host = Configure::read('HOST');
        $url = $host . '/api/timeline?' . http_build_query($options);
        $response = $this->http->get($url);

        return $response;
    }

    public function addPost(array $options)
    {
        $host = Configure::read('HOST');
        $url = $host . '/api/post/add';
        $response = $this->http->post($url, $options);

        return $response;
    }
}
