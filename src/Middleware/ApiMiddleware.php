<?php
namespace App\Middleware;

use Cake\Utility\Security;
use Cake\Core\Configure;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * Api middleware
 */
class ApiMiddleware
{
    /**
     * Invoke method.
     *
     * @param \Psr\Http\Message\ServerRequestInterface $request The request.
     * @param \Psr\Http\Message\ResponseInterface $response The response.
     * @param callable $next Callback to invoke the next middleware.
     * @return \Psr\Http\Message\ResponseInterface A response
     */
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, $next)
    {
        // this time Middle work first before the action of controller
        $response->getType('json');
        $hash = Security::hash(Configure::read('API_TOKEN'));

        if (isset($request->getHeaders()['User-Agent']) && $request->getHeaders()['User-Agent'][0] == 'ELB-HealthChecker/2.0') {
            $response = $response->withHeader('Content-Type', 'application/json')
                ->withHeader('Pragma', 'no-cache')
                ->withStatus(200);

            return $response;
        }

        if ($request->getHeaderLine('X-API-Token') !== $hash) {
            $response = $response->withHeader('Content-Type', 'application/json')
                ->withHeader('Pragma', 'no-cache')
                ->withStatus(401);

            // Write to the body
            $body = $response->getBody();
            // default error
            $error = [
                'code' => '401',
                'message' => __('Authentication Failed')
            ];
            $body->write(
                json_encode($error, JSON_UNESCAPED_UNICODE)
            );

            return $response;
        }
        // Calling $next() delegates control to the *next* middleware
        // In your application's queue.
        $response = $next($request, $response);

        return $response;
    }
}
