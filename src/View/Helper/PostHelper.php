<?php
namespace App\View\Helper;

use Cake\View\Helper;
use Cake\View\View;

/**
 * Post helper
 */
class PostHelper extends Helper
{
    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [];
    public $helpers = ['Html'];

        public function aging($strtime = null) {
        date_default_timezone_set("Asia/Manila");
        $date = date_create($strtime);
        $diff=date_diff($date,date_create(date('Y-m-d',strtotime('now'))));
        $to_time = strtotime("now");
        $from_time = strtotime($strtime);

        // Calculate the time difference of post_created upto today
        if ($diff->format('%a') > 365 ) {
            $datetime = $diff->format('%yy');
        } elseif ((abs($to_time - $from_time) / 86400) > 0.75) {
            $datetime = round(abs($to_time - $from_time) / 86400).'d';
        } elseif ((abs($to_time - $from_time) / 3600) > 0.75) {
            $datetime = round(abs($to_time - $from_time) / 3600). "h";
        } elseif ((abs($to_time - $from_time) / 60 ) > 0.75) {
            $datetime = round(abs($to_time - $from_time)/60)."m";
        } else {
            $datetime = round(abs($to_time - $from_time)+1)."s";
        }
        return $datetime;
    }

    public function hashtag($str = null) {
        $sub = [];
        $string = explode(" ", trim(strip_tags($str)));
        foreach ($string as $substr) {
            if (strpos($substr, '#') !== false) {
                $sub = preg_split("/#/",$substr,-1,PREG_SPLIT_DELIM_CAPTURE);
                array_splice($sub,0,1);
            }
        }

        if (!empty($sub)) {
            foreach ($sub as $index => $item) {
                if ($index == 0) {
                    $str = h($str);
                }
                $end = strpos(h($item),'&');
                if ( $end !== false) {
                    $item = substr($item,0,$end);
                }

                $str = str_replace('#'.$item,
                    $this->Html->link('#'.$item, [
                        'controller' => 'tags',
                        'action' => 'trends',$item,
                    ]),
                    str_replace("\n", "<br />",$str)
                );
            }
            return $str;
        }

        return h($str);
    }
}
